#include <bits/stdc++.h>

constexpr int P = 1e9 + 7;

int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = 1ll * r * a % P;
        a = 1ll * a * a % P;
        b >>= 1;
    }
    return r;
}

int inv[20];

int binom(int x, int y) {
    int res = 1;
    for (int i = 1; i <= y; ++i) res = 1ll * res * (x - i + 1) % P * inv[i] % P;
    return res;
}

int solve(const std::vector<int> &a, int m) {
    int n = a.size();
    int res = 0;
    std::function<void(int, int, int)> dfs = [&](int i, int c, int s) {
        if (s < 0) return;
        if (i == n) {
            res = (res + 1ll * (c % 2 ? P - 1 : 1) * binom(s + n - 1, n - 1)) % P;
            return;
        }
        dfs(i + 1, c, s);
        dfs(i + 1, c + 1, s - a[i] - 1);
    };
    dfs(0, 0, m);
    return res;
}

int L1[8], R1[8];
int L2[8], R2[8];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    inv[1] = 1;
    for (int i = 2; i < 20; ++i) inv[i] = 1ll * (P - P / i) * inv[P % i] % P;

    int T;
    std::cin >> T;
    while (T--) {
        int k1, k2;
        std::cin >> k1;
        for (int i = 0; i < k1; ++i) std::cin >> L1[i] >> R1[i];
        std::cin >> k2;
        for (int i = 0; i < k2; ++i) std::cin >> L2[i] >> R2[i];

        int deno = 1;
        for (int i = 0; i < k1; ++i) deno = 1ll * deno * (R1[i] - L1[i] + 1) % P;
        for (int i = 0; i < k2; ++i) deno = 1ll * deno * (R2[i] - L2[i] + 1) % P;
        deno = power(deno, P - 2);

        int ans1 = 0;
        int m = std::accumulate(R2, R2 + k2, 0) - std::accumulate(L1, L1 + k1, 0) - 1;
        std::vector<int> a(k1 + k2 + 1);
        for (int i = 0; i < k1; ++i) a[i] = R1[i] - L1[i];
        for (int i = 0; i < k2; ++i) a[i + k1] = R2[i] - L2[i];
        if (m >= 0) {
            a[k1 + k2] = m;
            ans1 = 1ll * solve(a, m) * deno % P;
        }

        int ans2 = 0;
        a.pop_back();
        ++m;
        if (m >= 0) {
            ans2 = 1ll * solve(a, m) * deno % P;
        }

        int ans3 = ((1 - ans1 + P) % P - ans2 + P) % P;

        std::cout << ans3 << ' ' << ans2 << ' ' << ans1 << '\n';
    }

    return 0;
}