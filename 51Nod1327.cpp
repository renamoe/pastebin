#include <bits/stdc++.h>

constexpr int P = 1000000007;

void inc(int &x, const int y) {
    x += y;
    if (x >= P) x -= P;
}

constexpr int N = 50, M = 200;

int left[M + 1];
int right[M + 1];
int mid[M + 1];
int binom[M + 1][M + 1];
int fac[M + 1];
int f[M + 1][M + 1][N + 1];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, m;
    std::cin >> n >> m;
    for (int i = 0; i < n; ++i) {
        int l, r;
        std::cin >> l >> r;
        ++left[l];
        ++right[m - r + 1];
        for (int i = l + 1; i <= m - r; ++i) {
            ++mid[i];
        }
    }

    for (int i = 0; i <= m; ++i) {
        binom[i][0] = 1;
        for (int j = 1; j <= i; ++j) {
            binom[i][j] = binom[i - 1][j - 1];
            inc(binom[i][j], binom[i - 1][j]);
        }
    }

    fac[0] = 1;
    for (int i = 1; i <= m; ++i) fac[i] = 1ll * fac[i - 1] * i % P;

    f[0][0][0] = 1;
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j <= i; ++j) {
            for (int k = 0; k <= n; ++k) {
                if (f[i][j][k] == 0) continue;

                if (j + 1 >= left[i + 1]) {
                    inc(f[i + 1][j + 1 - left[i + 1]][k + right[i + 1]], 
                        1ll * f[i][j][k] * binom[j + 1][left[i + 1]] % P * fac[left[i + 1]] % P);
                }
                if (j >= left[i + 1] && mid[i + 1] > 0) {
                    inc(f[i + 1][j - left[i + 1]][k + right[i + 1]], 
                        1ll * f[i][j][k] * binom[j][left[i + 1]] % P * fac[left[i + 1]] % P * mid[i + 1] % P);
                }
                if (j >= left[i + 1] && k + right[i + 1] > 0) {
                    inc(f[i + 1][j - left[i + 1]][k + right[i + 1] - 1], 
                        1ll * f[i][j][k] * binom[j][left[i + 1]] % P * fac[left[i + 1]] % P * (k + right[i + 1]) % P);
                }
            }
        }
    }

    int ans = 0;
    for (int j = 0; j <= m; ++j) {
        inc(ans, f[m][j][0]);
    }
    std::cout << ans << '\n';

    return 0;
}