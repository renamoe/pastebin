#include <bits/stdc++.h>

const int P = 998244353, R = 3;

int plus(const int x, const int y) {
    return (x + y >= P) ? (x + y - P) : (x + y);
}
int times(const int x, const int y) {
    return (long long) x * y % P;
}
int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = times(r, a);
        a = times(a, a);
        b >>= 1;
    }
    return r;
}

void dft(std::vector<int> &a) {
    static std::vector<int> rev, roots{0, 1};
    int n = a.size();
    if (int(rev.size()) != n) {
        int k = __builtin_ctz(n) - 1;
        rev.resize(n);
        for (int i = 0; i < n; ++i)
            rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << k);
    }
    for (int i = 0; i < n; ++i)
        if (rev[i] < i) std::swap(a[rev[i]], a[i]);
    if (int(roots.size()) < n) {
        int k = __builtin_ctz(roots.size());
        roots.resize(n);
        while ((1 << k) < n) {
            int wn = power(R, (P - 1) >> (k + 1));
            for (int i = 1 << (k - 1); i < (1 << k); ++i) {
                roots[i * 2] = roots[i];
                roots[i * 2 + 1] = times(roots[i], wn);
            }
            ++k;
        }
    }
    for (int k = 1; k < n; k *= 2) {
        for (int i = 0; i < n; i += k * 2) {
            for (int j = 0; j < k; ++j) {
                int x = a[i + j];
                int y = times(a[i + k + j], roots[k + j]);
                a[i + j] = plus(x, y);
                a[i + k + j] = plus(x, P - y);
            }
        }
    }
}

void idft(std::vector<int> &a) {
    int n = a.size();
    std::reverse(a.begin() + 1, a.end());
    dft(a);
    int invn = power(n, P - 2);
    for (int i = 0; i < n; ++i) a[i] = times(a[i], invn);
}

std::vector<int> juan(std::vector<int> a, std::vector<int> b, int lim) {
    int tot = a.size() + b.size() - 1;
    int n = 1;
    while (n < tot) n *= 2;
    std::vector<int> res(n);
    a.resize(n);
    b.resize(n);
    dft(a);
    dft(b);
    for (int i = 0; i < n; ++i) res[i] = times(a[i], b[i]);
    idft(res);
    res.resize(lim);
    return res;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int D, n;
    long long m;
    std::cin >> D >> n >> m;
    if ((long long) n < 2 * m) {
        std::cout << 0 << '\n';
        return 0;
    }
    int lim = 2 * D;
    std::vector<int> fac(lim + 1), ifac(lim + 1);
    fac[0] = ifac[0] = 1;
    for (int i = 1; i <= lim; ++i) fac[i] = times(fac[i - 1], i);
    ifac[lim] = power(fac[lim], P - 2);
    for (int i = lim; i > 1; --i) ifac[i - 1] = times(ifac[i], i);

    std::vector<int> f(D + 1);
    for (int i = 0; i <= D; ++i)
        f[i] = times((i % 2 ? (P - 1) : 1), times(power(plus(D, P - 2 * i), n), ifac[i]));
    f = juan(std::vector<int>(ifac.begin(), ifac.begin() + D + 1), f, D + 1);
    for (int i = 0; i <= D; ++i)
        f[i] = times(f[i], times(power(power(2, i), P - 2), times(fac[D], ifac[D - i])));
    
    for (int i = 0; i <= D; ++i) f[i] = times(f[i], fac[i]);
    std::reverse(f.begin(), f.end());
    std::vector<int> g(D + 1);
    for (int i = 0; i <= D; ++i)
        g[i] = times((i % 2 ? (P - 1) : 1), ifac[i]);
    g = juan(f, g, D + 1);
    int ans = 0;
    for (int i = std::max(0, int(D - n + 2 * m)); i <= D; ++i)
        ans = plus(ans, times(ifac[D - i], g[i]));
    std::cout << ans << '\n';
    
    return 0;
}