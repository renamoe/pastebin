#include <bits/stdc++.h>

constexpr double Pi = acos(-1.0);

struct Complex {
    double real, imag;
    Complex(double x = 0, double y = 0) : real(x), imag(y) {}
    friend Complex operator +(const Complex &x, const Complex &y) {
        return Complex(x.real + y.real, x.imag + y.imag);
    }
    friend Complex operator -(const Complex &x, const Complex &y) {
        return Complex(x.real - y.real, x.imag - y.imag);
    }
    friend Complex operator *(const Complex &x, const Complex &y) {
        return Complex(x.real * y.real - x.imag * y.imag, x.real * y.imag + x.imag * y.real);
    }
    Complex conj() const {
        return Complex(real, -imag);
    }
};

void dft(std::vector<Complex> &a) {
    static std::vector<int> rev;
    static std::vector<Complex> roots{Complex(0, 0), Complex(1, 0)};
    int n = a.size();
    if (int(rev.size()) != n) {
        int k = __builtin_ctz(n) - 1;
        rev.resize(n);
        for (int i = 0; i < n; ++i)
            rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << k);
    }
    for (int i = 0; i < n; ++i)
        if (rev[i] < i) std::swap(a[rev[i]], a[i]);
    if (int(roots.size()) < n) {
        int k = __builtin_ctz(roots.size());
        roots.resize(n);
        while ((1 << k) < n) {
            double d = Pi / double(1 << k);
            Complex w(std::cos(d), std::sin(d));
            for (int i = 1 << (k - 1); i < (1 << k); ++i) {
                roots[i * 2] = roots[i];
                roots[i * 2 + 1] = roots[i] * w;
            }
            ++k;
        }
    }
    for (int k = 1; k < n; k *= 2) {
        for (int i = 0; i < n; i += k * 2) {
            for (int j = 0; j < k; ++j) {
                Complex x = a[i + j];
                Complex y = a[i + k + j] * roots[k + j];
                a[i + j] = x + y;
                a[i + k + j] = x - y;
            }
        }
    }
}

void idft(std::vector<Complex> &a) {
    int n = a.size();
    std::reverse(a.begin() + 1, a.end());
    dft(a);
    for (int i = 0; i < n; ++i) {
        a[i].real /= n;
        a[i].imag /= n;
    }
}

std::vector<int> conv(const std::vector<int> &f, const std::vector<int> &g, int p, int lim) {
    int tot = f.size() + g.size() - 1;
    int n = 1;
    while (n < tot) n *= 2;
    std::vector<Complex> a(n), b(n), c(n), d(n);
    for (int i = 0; i < int(f.size()); ++i) a[i] = Complex(f[i] >> 15, f[i] & 32767);
    for (int i = 0; i < int(g.size()); ++i) c[i] = Complex(g[i] >> 15, g[i] & 32767);
    dft(a);
    dft(c);
    for (int i = 0; i < n; ++i) b[i] = a[i].conj();
    std::reverse(b.begin() + 1, b.end());
    for (int i = 0; i < n; ++i) d[i] = c[i].conj();
    std::reverse(d.begin() + 1, d.end());
    for (int i = 0; i < n; ++i) {
        Complex aa = (a[i] + b[i]) * Complex(0.5, 0);
        Complex bb = (a[i] - b[i]) * Complex(0, -0.5);
        Complex cc = (c[i] + d[i]) * Complex(0.5, 0);
        Complex dd = (c[i] - d[i]) * Complex(0, -0.5);
        a[i] = aa * cc + Complex(0, 1) * (aa * dd + bb * cc);
        b[i] = bb * dd;
    }
    idft(a);
    idft(b);
    std::vector<int> res(tot);
    for (int i = 0; i < tot; ++i) {
        int t1 = (long long)(a[i].real + 0.5) % p;
        int t2 = (long long)(a[i].imag + 0.5) % p;
        int t3 = (long long)(b[i].real + 0.5) % p;
        res[i] = (((1ll << 30) * t1 + (1ll << 15) * t2 + t3) % p + p) % p;
    }
    res.resize(lim);
    return res;
}

std::vector<int> poly_inv(std::vector<int> a, int p, int lim) {
    int n = 1;
    while (n < int(a.size())) n *= 2;
    a.resize(n);
    assert(a[0] == 1);
    std::vector<int> b{1};
    int k = 1;
    while (k < n) {
        k *= 2;
        auto bs = b;
        b = conv(std::vector<int>(a.begin(), a.begin() + k), conv(b, b, p, k), p, k);
        for (int i = 0; i < k; ++i)
            b[i] = ((i < k / 2 ? (2ll * bs[i] % p) : 0) - b[i] + p) % p;
    }
    b.resize(lim);
    return b;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, m, p;
    std::cin >> n >> m >> p;
    std::vector<std::array<int, 18>> f(m + 1, std::array<int, 18>{});
    for (int i = 1; i <= m; ++i) {
        f[i][1] = 1;
        for (int j = i * 2; j <= m; j += i)
            for (int k = 2; k < 18; ++k)
                f[j][k] = (f[j][k] + f[i][k - 1]) % p;
    }
    int sum[18] = {};
    for (int i = 1; i <= m; ++i)
        for (int j = 1; j < 18; ++j)
            sum[j] = (sum[j] + f[i][j]) % p;
    std::vector<std::array<int, 18>> comb(n, std::array<int, 18>{});
    for (int i = 0; i < n; ++i) {
        comb[i][0] = 1;
        for (int j = 1; j <= i && j < 18; ++j)
            comb[i][j] = (comb[i - 1][j - 1] + comb[i - 1][j]) % p;
    }
    std::vector<int> G(n + 1);
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= i && j < 18; ++j)
            G[i] = (G[i] + 1ll * sum[j] * comb[i - 1][j - 1]) % p;
        if (i % 2 == 0) G[i] = (p - G[i]) % p;
    }
    // for (int i = 0; i <= n; ++i)
    //     std::cerr << G[i] << " \n"[i == n];
    for (int i = 1; i <= n; ++i) G[i] = (p - G[i]) % p;
    ++G[0];
    auto F = poly_inv(G, p, n + 1);
    std::cout << F[n] << '\n';
    
    return 0;
}