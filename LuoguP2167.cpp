#include <bits/stdc++.h>

constexpr int P = 1000003;

int binom[16][16];
char s[15][52];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    for (int i = 0; i <= 15; ++i) {
        binom[i][0] = 1;
        for (int j = 1; j <= i; ++j) {
            binom[i][j] = (binom[i - 1][j - 1] + binom[i - 1][j]) % P;
        }
    }

    int T;
    std::cin >> T;
    while (T--) {
        int n, k;
        std::cin >> n >> k;
        for (int i = 0; i < n; ++i) std::cin >> s[i];
        if (k > n) {
            std::cout << 0 << '\n';
            continue;
        }
        int m = std::strlen(s[0]);
        int ans = 0;
        for (int S = 0; S < (1 << n); ++S) {
            int size = __builtin_popcount(S);
            if (size >= k) {
                int sum = 1;
                for (int i = 0; i < m; ++i) {
                    char t = 0;
                    int now = 26;
                    for (int j = 0; j < n; ++j) {
                        if ((S >> j) & 1) {
                            if (s[j][i] != '?') {
                                if (t == 0 || s[j][i] == t) {
                                    t = s[j][i];
                                    now = 1;
                                } else {
                                    now = 0;
                                    break;
                                }
                            }
                        }
                    }
                    sum = 1ll * sum * now % P;
                }
                ans = (ans + 1ll * ((size - k) % 2 ? P - 1 : 1) * sum % P * binom[size][k] % P) % P;
            }
        }
        std::cout << ans << '\n';
    }

    return 0;
}