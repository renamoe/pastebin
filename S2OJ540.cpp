#include <bits/stdc++.h>

using i64 = long long;

const int Inf = 0x3f3f3f3f;

struct SegmentTree {
    std::vector<std::pair<int, int>> mi;
    SegmentTree(int n) {
        mi.resize(n * 4, {Inf, Inf});
    }
    void range_cmin(int u, int l, int r, int L, int R, std::pair<int, int> k) {
        if (L <= l && r <= R) {
            mi[u] = std::min(mi[u], k);
            return;
        }
        int mid = (l + r) / 2;
        if (L < mid) range_cmin(u * 2, l, mid, L, R, k);
        if (R > mid) range_cmin(u * 2 + 1, mid, r, L, R, k);
    }
    std::pair<int, int> query(int u, int l, int r, int p) {
        if (l == r - 1)
            return mi[u];
        int mid = (l + r) / 2;
        if (p < mid) return std::min(mi[u], query(u * 2, l, mid, p));
        else return std::min(mi[u], query(u * 2 + 1, mid, r, p));
    }
};

bool cmp(const std::pair<int, int> &x, const std::pair<int, int> &y) {
    return (i64) x.first * y.second < (i64) y.first * x.second;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int Q;
    std::cin >> Q;
    std::vector<int> x0(Q), y0(Q), x1(Q), y1(Q), op(Q);
    for (int i = 0; i < Q; ++i) {
        std::cin >> op[i];
        if (op[i] == 1) {
            std::cin >> x0[i] >> y0[i] >> x1[i] >> y1[i];
        } else {
            std::cin >> x0[i] >> y0[i];
        }
    }
    std::vector<std::pair<double, int>> ans(Q, {1e18, Inf});
    
    /* 竖着的 */ {
        std::vector<std::pair<int, int>> hs;
        for (int i = 0; i < Q; ++i) {
            if (x0[i] != 0) {
                hs.emplace_back(y0[i], x0[i]);
                if (op[1] == 1)
                    hs.emplace_back(y1[i], x0[i]);
            }
        }
        std::sort(hs.begin(), hs.end(), cmp);
        std::unique(hs.begin(), hs.end(), [](auto x, auto y) {
            return (i64) x.first * y.second == (i64) y.first * x.second;
        });
        int n = hs.size();
        SegmentTree tr(n);
        for (int i = 0; i < Q; ++i) {
            if (x0[i] == 0) continue;
            if (op[i] == 1) {
                int l = std::lower_bound(hs.begin(), hs.end(), std::make_pair(y0[i], x0[i]), cmp) - hs.begin();
                int r = std::lower_bound(hs.begin(), hs.end(), std::make_pair(y1[i], x0[i]), cmp) - hs.begin() + 1;
                tr.range_cmin(1, 0, n, l, r, {x0[i], -(i + 1)});
            } else {
                int p = std::lower_bound(hs.begin(), hs.end(), std::make_pair(y0[i], x0[i]), cmp) - hs.begin();
                auto res = tr.query(1, 0, n, p);
                if (res.second != Inf) {
                    ans[i] = std::min(ans[i], std::make_pair((double) res.first / x0[i], res.second));
                }
            }
        }
    }
    
    /* 横着的 */ {
        std::vector<std::pair<int, int>> hs;
        for (int i = 0; i < Q; ++i) {
            if (y0[i] != 0) {
                hs.emplace_back(x0[i], y0[i]);
                if (op[1] == 1)
                    hs.emplace_back(x1[i], y0[i]);
            }
        }
        std::sort(hs.begin(), hs.end(), cmp);
        std::unique(hs.begin(), hs.end(), [](auto x, auto y) {
            return (i64) x.first * y.second == (i64) y.first * x.second;
        });
        int n = hs.size();
        SegmentTree tr(n);
        for (int i = 0; i < Q; ++i) {
            if (y0[i] == 0) continue;
            if (op[i] == 1) {
                int l = std::lower_bound(hs.begin(), hs.end(), std::make_pair(x0[i], y0[i]), cmp) - hs.begin();
                int r = std::lower_bound(hs.begin(), hs.end(), std::make_pair(x1[i], y0[i]), cmp) - hs.begin() + 1;
                tr.range_cmin(1, 0, n, l, r, {y0[i], -(i + 1)});
            } else {
                int p = std::lower_bound(hs.begin(), hs.end(), std::make_pair(x0[i], y0[i]), cmp) - hs.begin();
                auto res = tr.query(1, 0, n, p);
                if (res.second != Inf) {
                    ans[i] = std::min(ans[i], std::make_pair((double) res.first / y0[i], res.second));
                }
            }
        }
    }
    
    for (int i = 0; i < Q; ++i) {
        if (op[i] == 2) {
            if (ans[i].second == Inf) std::cout << 0 << '\n';
            else std::cout << -ans[i].second << '\n';
        }
    }
    
    return 0;
}