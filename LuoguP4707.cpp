#include <bits/stdc++.h>

constexpr int P = 998244353;

void inc(int &x, const int y) {
    x += y;
    if (x >= P) x -= P;
}

constexpr int M = 10000;

int f[M + 1][12];
int inv[M + 1];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, K, m;
    std::cin >> n >> K >> m;
    K = n - K + 1;
    f[0][0] = 1;
    for (int i = 1; i <= n; ++i) {
        int w;
        std::cin >> w;
        for (int j = m; j >= 0; --j) {
            if (j >= w) {
                for (int k = 1; k <= K; ++k) {
                    inc(f[j][k], P - f[j - w][k]);
                    inc(f[j][k], f[j - w][k - 1]);
                }
            }
        }
    }

    inv[1] = 1;
    for (int i = 2; i <= m; ++i) inv[i] = 1ll * (P - P / i) * inv[P % i] % P;

    int ans = 0;
    for (int i = 1; i <= m; ++i) {
        inc(ans, 1ll * f[i][K] * inv[i] % P);
    }
    ans = 1ll * ans * m % P;
    std::cout << ans << '\n';

    return 0;
}