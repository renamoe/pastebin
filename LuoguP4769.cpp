#include <bits/stdc++.h>

const int N = 1200000;
const int P = 998244353;

int plus(const int x, const int y) {
    return (x + y >= P) ? (x + y - P) : (x + y);
}
int times(const int x, const int y) {
    return (long long) x * y % P;
}

int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = times(r, a);
        a = times(a, a);
        b >>= 1;
    }
    return r;
}

int fac[N + 1], ifac[N + 1];

int comb(int x, int y) {
    if (x < 0 || y < 0 || x < y) return 0;
    return times(fac[x], times(ifac[y], ifac[x - y]));
}
int f(int x, int y) {
    if (x == 0)
        return 1;
    return plus(comb(x + y, x), P - comb(x + y, x - 1));
}

int main() {
    fac[0] = ifac[0] = 1;
    for (int i = 1; i <= N; ++i) fac[i] = times(fac[i - 1], i);
    ifac[N] = power(fac[N], P - 2);
    for (int i = N; i > 1; --i) ifac[i - 1] = times(ifac[i], i);

    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int T;
    std::cin >> T;
    while (T--) {
        int n;
        std::cin >> n;
        std::vector<int> a(n + 1), mi(n + 1);
        for (int i = 1; i <= n; ++i) std::cin >> a[i];
        mi[n] = a[n];
        for (int i = n - 1; i >= 1; --i)
            mi[i] = std::min(a[i], mi[i + 1]);
        int ans = 0;
        int ma = 0;
        for (int i = 1; i <= n; ++i) {
            int x = std::max(ma + 1, a[i] + 1);
            if (x <= n) {
                ans = plus(ans, f(n - x, n - i + 1));
            }
            if (a[i] > ma) {
                ma = a[i];
            } else if (a[i] != mi[i]) {
                break;
            }
        }
        std::cout << ans << '\n';
    }
    
    return 0;
}