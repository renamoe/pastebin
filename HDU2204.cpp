#include <cmath>
#include <iostream>

typedef long long i64;

const int pri[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59};

i64 n;
i64 ans;

void dfs(int i, int s, int c) {
    i64 rt = std::pow(n, 1.0 / s);
    if (rt > 1) {
        ans += (c % 2 ? 1 : -1) * (rt - 1);
    }
    if (c < 3) {
        for (int j = i + 1; j < 17; ++j) {
            dfs(j, s * pri[j], c + 1);
        }
    }
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(NULL);

    while (std::cin >> n) {
        ans = 1;
        for (int i = 0; i < 17; ++i) {
            dfs(i, pri[i], 1);
        }
        std::cout << ans << '\n';
    }

    return 0;
}