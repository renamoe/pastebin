// 打表代码

#include <bits/stdc++.h>

constexpr int P = 998244353;

int dp[2][1 << 27];
int inv[29];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    
    dp[0][0] = 1;
    int *f = dp[0];
    int *g = dp[1];
    for (int i = 1; i <= n - 1; ++i) {
        std::fill_n(g, 1 << i, 0);
        for (int s = 0; s < (1 << (i - 1)); ++s) {
            g[s << 1] += f[s];
            if (g[s << 1] >= P) g[s << 1] -= P;
            for (int j = 0; j < i; ++j) {
                int suf = s >> j;
                if (suf) suf ^= suf & -suf;
                int t = (s & ((1 << j) - 1)) | (1 << j) | (suf << (j + 1));
                g[t] += f[s];
                if (g[t] >= P) g[t] -= P;
            }
        }
        std::swap(f, g);
    }

    int ans = 0;
    for (int s = 0; s < (1 << (n - 1)); ++s) {
        ans = (ans + 1ll * f[s] * (1 + int(__builtin_popcount(s)))) % P;
    }
    inv[1] = 1;
    for (int i = 2; i <= n; ++i) inv[i] = 1ll * (P - P / i) * inv[P % i] % P;
    for (int i = 1; i <= n; ++i) ans = 1ll * ans * inv[i] % P;
    std::cout << ans << '\n';

    return 0;
}
