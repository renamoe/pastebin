#include <bits/stdc++.h>

using i64 = long long;

constexpr int N = 100000;

int a[N];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, W;
    std::cin >> n >> W;
    for (int i = 0; i < n; ++i) std::cin >> a[i];

    i64 min_ans = 0;
    int ma = *std::max_element(a, a + n);
    int mi = *std::min_element(a, a + n);
    if (ma > W) min_ans += ma - W;
    if (mi < W) min_ans += W - mi;

    i64 max_ans = 0;
    std::sort(a, a + n);
    int l = 0;
    int r = n - 1;
    i64 sum = std::abs(a[0] - W);
    int op = 0;
    for (int i = 0; i < n - 1; ++i) {
        if (op == 0) {
            sum += std::max(std::abs(a[r] - a[l]), std::abs(a[r] - W));
            ++l;
            op = 1;
        } else {
            sum += std::max(std::abs(a[l] - a[r]), std::abs(a[l] - W));
            --r;
            op = 0;
        }
    }
    max_ans = std::max(max_ans, sum);

    l = 0;
    r = n - 1;
    sum = std::abs(a[r] - W);
    op = 1;
    for (int i = 0; i < n - 1; ++i) {
        if (op == 0) {
            sum += std::max(std::abs(a[r] - a[l]), std::abs(a[r] - W));
            ++l;
            op = 1;
        } else {
            sum += std::max(std::abs(a[l] - a[r]), std::abs(a[l] - W));
            --r;
            op = 0;
        }
    }
    max_ans = std::max(max_ans, sum);

    std::cout << min_ans << ' ' << max_ans << '\n';


    return 0;
}