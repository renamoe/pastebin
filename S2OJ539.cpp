#include <bits/stdc++.h>

using f64 = double;

const f64 Eps = 1e-8;

int cmp(f64 k) {
    if (fabs(k) < Eps) return 0;
    return k > 0 ? 1 : -1;
}

struct Vector {
    f64 x, y;
    Vector(f64 x = 0, f64 y = 0) : x(x), y(y) {}
    friend bool operator <(const Vector &a, const Vector &b) {
        if (cmp(a.x - b.x) == 0) return cmp(a.y - b.y) < 0;
        return cmp(a.x - b.x) < 0;
    }
    friend bool operator ==(const Vector &a, const Vector &b) {
        return cmp(a.x - b.x) == 0 && cmp(a.y - b.y) == 0;
    }
    friend Vector operator +(const Vector &a, const Vector &b) {
        return Vector(a.x + b.x, a.y + b.y);
    }
    friend Vector operator -(const Vector &a, const Vector &b) {
        return Vector(a.x - b.x, a.y - b.y);
    }
    friend Vector operator *(const Vector &a, const f64 &k) {
        return Vector(a.x * k, a.y * k);
    }
    friend Vector operator /(const Vector &a, const f64 &k) {
        return Vector(a.x / k, a.y / k);
    }
    friend f64 operator *(const Vector &a, const Vector &b) {
        return a.x * b.x + a.y * b.y;
    }
    friend f64 operator ^(const Vector &a, const Vector &b) {
        return a.x * b.y - a.y * b.x;
    }
};

struct Line {
    Vector s, t;
    Line() {}
    Line(Vector s, Vector t) : s(s), t(t) {}
    bool in(const Vector &p) const {
        return cmp((s - p) ^ (t - p)) == 0 && cmp((s - p) * (t - p)) < 0;
    }
    friend std::pair<Vector, bool> inter(const Line &lhs, const Line &rhs) {
        Vector a = lhs.s, b = lhs.t;
        Vector c = rhs.s, d = rhs.t;
        f64 Sabc = (a - c) ^ (b - c);
        f64 Sabd = (b - d) ^ (a - d);
        if (cmp(Sabc * Sabd) <= 0) return {Vector(), false};
        f64 Scda = (c - a) ^ (d - a);
        f64 Scdb = (d - b) ^ (c - b);
        if (cmp(Scda * Scdb) <= 0) return {Vector(), false};
        Vector p = c + (d - c) * (Sabc / (Sabc + Sabd));
        return {p, true};
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);
    
    int n;
    while (std::cin >> n && n) {
        std::vector<Vector> a(n);
        for (int i = 0; i < n; ++i) {
            int x, y;
            std::cin >> x >> y;
            a[i].x = x;
            a[i].y = y;
        }
        std::vector<Line> l(n - 1);
        for (int i = 0; i < n - 1; ++i)
            l[i] = Line(a[i], a[i + 1]);
        for (int i = 0; i < n - 1; ++i)
            for (int j = i + 1; j < n - 1; ++j) {
                auto res = inter(l[i], l[j]);
                if (res.second) a.push_back(res.first);
            }
        std::sort(a.begin(), a.end());
        a.erase(std::unique(a.begin(), a.end()), a.end());
        int V = a.size();
        int E = n - 1;
        for (auto p : a)
            for (int i = 0; i < n - 1; ++i)
                if (l[i].in(p))
                    ++E;
        std::cout << (E + 2 - V) << '\n';
    }
    
    return 0;
}