#include <bits/stdc++.h>

using u32 = unsigned int;
using u64 = unsigned long long;

constexpr int N = int(1e7);

u32 P;
u32 inv_2, inv_4, inv_6;

u32 plus(const u32 x, const u32 y) {
    return x + y >= P ? (x + y - P) : (x + y);
}

void inc(u32 &x, const u32 y) {
    x += y;
    if (x >= P) x -= P;
}

u32 power(u32 a, u32 b) {
    u32 r = 1;
    while (b) {
        if (b & 1u) r = 1ull * r * a % P;
        a = 1ull * a * a % P;
        b >>= 1;
    }
    return r;
}

int phi[N + 1];
bool np[N + 1];
std::vector<int> pri;

u32 pre_sum[N + 1];
std::map<u64, u32> mem;

u32 S1(u64 n) {
    n %= P;
    return 1ull * n * (n + 1) % P * inv_2 % P;
}

u32 S2(u64 n) {
    n %= P;
    return 1ull * n * (n + 1) % P * ((2 * n + 1) % P) % P * inv_6 % P;
}

u32 S3(u64 n) {
    n %= P;
    return 1ull * n * n % P * (n + 1) % P * (n + 1) % P * inv_4 % P;
}

u32 F(u64 n) {
    if (n <= N) return pre_sum[n];
    if (mem.count(n)) return mem[n];
    u32 res = S3(n);
    for (u64 l = 2, r; l <= n; l = r + 1) {
        r = n / (n / l);
        inc(res, P - 1ull * F(n / l) * plus(P - S2(l - 1), S2(r)) % P);
    }
    return mem[n] = res;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::cin >> P;

    inv_2 = power(2, P - 2);
    inv_4 = power(4, P - 2);
    inv_6 = power(6, P - 2);

    phi[1] = 1;
    for (int i = 2; i <= N; ++i) {
        if (!np[i]) {
            pri.push_back(i);
            phi[i] = i - 1;
        }
        for (int j : pri) {
            int k = i * j;
            if (k > N) break;
            np[k] = true;
            if (i % j != 0) {
                phi[k] = phi[i] * (j - 1);
            } else {
                phi[k] = phi[i] * j;
                break;
            }
        }
    }

    for (int i = 1; i <= N; ++i) {
        pre_sum[i] = (pre_sum[i - 1] + 1ull * phi[i] * i % P * i) % P;
    }

    u64 n;
    std::cin >> n;
    u32 ans = 0;
    for (u64 l = 1, r; l <= n; l = r + 1) {
        r = n / (n / l);
        u32 s = S1(n / l);
        inc(ans, 1ull * s * s % P * plus(F(r), P - F(l - 1)) % P);
    }
    std::cout << ans << '\n';

    return 0;
}