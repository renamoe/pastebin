#include <bits/stdc++.h>

using i64 = long long;

const i64 Inf = 1e12;

struct Status {
    i64 val;
    int cnt;
    Status(i64 x = 0, int y = 0) : val(x), cnt(y) {}
    friend Status operator +(const Status &x, const Status &y) {
        return Status(x.val + y.val, x.cnt + y.cnt);
    }
    friend bool operator <(const Status &x, const Status &y) {
        return x.val < y.val || (x.val == y.val && x.cnt > y.cnt);
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, k;
    std::cin >> n >> k;
    ++k;
    std::vector<std::vector<std::pair<int, int>>> e(n);
    for (int i = 0; i < n - 1; ++i) {
        int u, v, w;
        std::cin >> u >> v >> w;
        --u;
        --v;
        e[u].emplace_back(v, w);
        e[v].emplace_back(u, w);
    }
    auto check = [&](i64 dt) -> Status {
        std::vector<Status> f0(n), f1(n), f2(n);
        std::function<void(int, int)> dp = [&](int u, int fa) {
            for (auto i : e[u]) {
                int v, w;
                std::tie(v, w) = i;
                if (v == fa) continue;
                dp(v, u);
                f2[u] = std::max(f2[u] + f0[v], f1[u] + f1[v] + Status(w - dt, 1));
                f1[u] = std::max(f1[u] + f0[v], f0[u] + f1[v] + Status(w, 0));
                f0[u] = f0[u] + f0[v];
            }
            f0[u] = std::max(f0[u], std::max(f1[u] + Status(-dt, 1), f2[u]));
        };
        dp(0, -1);
        return f0[0];
    };
    i64 l = -Inf, r = Inf;
    while (r - l > 1) {
        i64 mid = (l + r) / 2;
        if (check(mid).cnt <= k) r = mid;
        else l = mid;
    }
    std::cerr << r << '\n';
    i64 ans = check(r).val + k * r;
    std::cout << ans << '\n';
    
    return 0;
}