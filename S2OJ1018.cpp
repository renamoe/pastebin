#include <bits/stdc++.h>

constexpr int P = 1e9 + 7;

void inc(int &x, const int y) {
    x += y;
    if (x >= P) x -= P;
}

void fwt_or(std::vector<int> &f) {
    int n = f.size();
    for (int mid = 1; mid < n; mid *= 2)
        for (int i = 0; i < n; i += mid * 2)
            for (int j = 0; j < mid; ++j)
                inc(f[i + mid + j], f[i + j]);
}

void ifwt_or(std::vector<int> &f) {
    int n = f.size();
    for (int mid = 1; mid < n; mid *= 2)
        for (int i = 0; i < n; i += mid * 2)
            for (int j = 0; j < mid; ++j)
                inc(f[i + mid + j], P - f[i + j]);
}

std::vector<int> conv_or(std::vector<int> a, std::vector<int> b) {
    fwt_or(a);
    fwt_or(b);
    for (int i = 0; i < int(a.size()); ++i) a[i] = 1ll * a[i] * b[i] % P;
    ifwt_or(a);
    return a;
}

void fwt_and(std::vector<int> &f) {
    int n = f.size();
    for (int mid = 1; mid < n; mid *= 2)
        for (int i = 0; i < n; i += mid * 2)
            for (int j = 0; j < mid; ++j)
                inc(f[i + j], f[i + mid + j]);
}

void ifwt_and(std::vector<int> &f) {
    int n = f.size();
    for (int mid = 1; mid < n; mid *= 2)
        for (int i = 0; i < n; i += mid * 2)
            for (int j = 0; j < mid; ++j)
                inc(f[i + j], P - f[i + mid + j]);
}

std::vector<int> conv_and(std::vector<int> a, std::vector<int> b) {
    fwt_and(a);
    fwt_and(b);
    for (int i = 0; i < int(a.size()); ++i) a[i] = 1ll * a[i] * b[i] % P;
    ifwt_and(a);
    return a;
}

std::vector<int> operator +(std::vector<int> a, const std::vector<int> &b) {
    for (int i = 0; i < int(a.size()); ++i) inc(a[i], b[i]);
    return a;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::string a;
    std::cin >> a;
    int n = a.length();
    int m;
    std::cin >> m;
    std::vector<int> b(m);
    int ed = 0;
    for (int i = 0; i < m; ++i) {
        for (int j = 0; j < 4; ++j) {
            int t;
            std::cin >> t;
            b[i] |= t << j;
        }
        int t;
        std::cin >> t;
        ed |= t << i;
    }
    std::function<std::vector<int> *(int, int)> solve = [&](int l, int r) {
        std::vector<int> *u = new std::vector<int>();
        if (l == r - 1) {
            u->resize(1 << m);
            if (a[l] == '?') {
                for (int c = 0; c < 4; ++c) {
                    int s = 0;
                    for (int i = 0; i < m; ++i) s |= ((b[i] >> c) & 1) << i;
                    ++(*u)[s];
                }
                for (int c = 0; c < 4; ++c) {
                    int s = 0;
                    for (int i = 0; i < m; ++i) s |= (((b[i] >> c) & 1) ^ 1) << i;
                    ++(*u)[s];
                }
            } else if (a[l] >= 'A' && a[l] <= 'D') {
                int c = a[l] - 'A';
                int s = 0;
                for (int i = 0; i < m; ++i) s |= ((b[i] >> c) & 1) << i;
                ++(*u)[s];
            } else {
                int c = a[l] - 'a';
                int s = 0;
                for (int i = 0; i < m; ++i) s |= (((b[i] >> c) & 1) ^ 1) << i;
                ++(*u)[s];
            }
            return u;
        }
        int mid = -1;
        int top = 0;
        for (int i = l; i < r; ++i) {
            if (a[i] == '(') {
                ++top;
            } else if (a[i] == ')') {
                --top;
            } else if (top == 0) {
                mid = i;
                break;
            }
        }
        auto ls = solve(l + 1, mid - 1);
        auto rs = solve(mid + 2, r - 1);
        if (a[mid] == '?') {
            *u = conv_or(*ls, *rs) + conv_and(*ls, *rs);
        } else if (a[mid] == '|') {
            *u = conv_or(*ls, *rs);
        } else {
            *u = conv_and(*ls, *rs);
        }
        delete ls;
        delete rs;
        return u;
    };

    auto root = solve(0, n);
    std::cout << (*root)[ed] << '\n';
    delete root;

    return 0;
}