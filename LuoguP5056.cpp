#include <bits/stdc++.h>

using i64 = long long;

struct HashTable {
    static const int P = 23333;
    int cnt;
    int key[P + 1], id[P];
    i64 val[P];
    HashTable() { clear(); }
    void clear() {
        cnt = 0;
        std::fill(id, id + P, 0);
        std::fill(key, key + P + 1, 0);
        std::fill(val, val + P, 0);
    }
    i64 &operator [](const int k) {
        int i = k % P;
        while (id[i]) {
            if (key[id[i]] == k)
                return val[id[i]];
            ++i;
            if (i == P) i = 0;
        }
        ++cnt;
        id[i] = cnt;
        key[cnt] = k;
        return val[cnt];
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, m;
    std::cin >> n >> m;
    std::vector<std::vector<int>> a(n + 1, std::vector<int>(m + 1));
    std::string str;
    int end_x = -1, end_y = -1;
    for (int i = 0; i < n; ++i) {
        std::cin >> str;
        for (int j = 0; j < m; ++j)
            if (str[j] == '.') {
                a[i][j] = true;
                end_x = i;
                end_y = j;
            }
    }
    i64 ans = 0;
    static HashTable dp[2];
    dp[0].clear();
    dp[0][0] = 1;
    int cur = 0;
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            auto &f = dp[cur];
            auto &g = dp[cur ^ 1];
            g.clear();
            for (int t = 1; t <= f.cnt; ++t) {
                int s = f.key[t];
                i64 now = f.val[t];
                int l = (s >> (j * 2)) & 3;
                int r = (s >> (j * 2 + 2)) & 3;
                if (!a[i][j]) {
                    if (!l && !r)
                        g[s] += now;
                    continue;
                }
                if (!l && !r) {
                    if (a[i + 1][j] && a[i][j + 1])
                        g[s ^ (1 << (j * 2)) ^ (2 << (j * 2 + 2))] += now;
                } else if (l && !r) {
                    if (a[i][j + 1])
                        g[s ^ (l << (j * 2)) ^ (l << (j * 2 + 2))] += now;
                    if (a[i + 1][j])
                        g[s] += now;
                } else if (!l && r) {
                    if (a[i + 1][j])
                        g[s ^ (r << (j * 2)) ^ (r << (j * 2 + 2))] += now;
                    if (a[i][j + 1])
                        g[s] += now;
                } else if (l == 1 && r == 1) {
                    int k = j + 1, c = 0;
                    while (k <= m) {
                        int x = (s >> (k * 2)) & 3;
                        if (x == 1)
                            ++c;
                        else if (x == 2)
                            --c;
                        if (c == 0)
                            break;
                        ++k;
                    }
                    g[s ^ (5 << (j * 2)) ^ (3 << (k * 2))] += now;
                } else if (l == 2 && r == 2) {
                    int k = j, c = 0;
                    while (k >= 0) {
                        int x = (s >> (k * 2)) & 3;
                        if (x == 2)
                            ++c;
                        else if (x == 1)
                            --c;
                        if (c == 0)
                            break;
                        --k;
                    }
                    g[s ^ (10 << (j * 2)) ^ (3 << (k * 2))] += now;
                } else if (l == 2 && r == 1) {
                    g[s ^ (6 << (j * 2))] += now;
                } else if (l == 1 && r == 2) {
                    if (i == end_x && j == end_y)
                        ans += now;
                }
            }
            cur ^= 1;
        }
        for (int t = 1; t <= dp[cur].cnt; ++t)
            dp[cur].key[t] <<= 2;
    }
    std::cout << ans << '\n';
    
    return 0;
}