#include <bits/stdc++.h>

const int P = 1004535809, R = 3;

int plus(const int x, const int y) {
    return (x + y >= P) ? (x + y - P) : (x + y);
}
int times(const int x, const int y) {
    return (long long) x * y % P;
}
int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = times(r, a);
        a = times(a, a);
        b >>= 1;
    }
    return r;
}

void dft(std::vector<int> &a) {
    static std::vector<int> rev, roots{0, 1};
    int n = a.size();
    if (int(rev.size()) != n) {
        int k = __builtin_ctz(n) - 1;
        rev.resize(n);
        for (int i = 0; i < n; ++i)
            rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << k);
    }
    for (int i = 0; i < n; ++i)
        if (rev[i] < i) std::swap(a[rev[i]], a[i]);
    if (int(roots.size()) < n) {
        int k = __builtin_ctz(roots.size());
        roots.resize(n);
        while ((1 << k) < n) {
            int wn = power(R, (P - 1) >> (k + 1));
            for (int i = 1 << (k - 1); i < (1 << k); ++i) {
                roots[i * 2] = roots[i];
                roots[i * 2 + 1] = times(roots[i], wn);
            }
            ++k;
        }
    }
    for (int k = 1; k < n; k *= 2) {
        for (int i = 0; i < n; i += k * 2) {
            for (int j = 0; j < k; ++j) {
                int x = a[i + j];
                int y = times(a[i + k + j], roots[k + j]);
                a[i + j] = plus(x, y);
                a[i + k + j] = plus(x, P - y);
            }
        }
    }
}

void idft(std::vector<int> &a) {
    int n = a.size();
    std::reverse(a.begin() + 1, a.end());
    dft(a);
    int invn = power(n, P - 2);
    for (int i = 0; i < n; ++i) a[i] = times(a[i], invn);
}

std::vector<int> conv(std::vector<int> a, std::vector<int> b, int lim) {
    int tot = a.size() + b.size() - 1;
    int n = 1;
    while (n < tot) n *= 2;
    std::vector<int> res(n);
    a.resize(n);
    b.resize(n);
    dft(a);
    dft(b);
    for (int i = 0; i < n; ++i) res[i] = times(a[i], b[i]);
    idft(res);
    res.resize(lim);
    return res;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, m, s;
    std::cin >> n >> m >> s;
    std::vector<int> val(m + 1);
    for (int i = 0; i <= m; ++i) std::cin >> val[i];
    
    int N = std::max(n, m);
    std::vector<int> fac(N + 1), ifac(N + 1);
    fac[0] = ifac[0] = 1;
    for (int i = 1; i <= N; ++i) fac[i] = times(fac[i - 1], i);
    ifac[N] = power(fac[N], P - 2);
    for (int i = N; i > 1; --i) ifac[i - 1] = times(ifac[i], i);
    auto comb = [&](int x, int y) -> int {
        return times(fac[x], times(ifac[y], ifac[x - y]));
    };

    std::vector<int> g(m + 1), f(m + 1);
    for (int i = 0; i <= std::min(m, n / s); ++i)
        g[i] = times(fac[i], times(comb(m, i), times(fac[n], times(power(times(power(fac[s], i), fac[n - i * s]), P - 2), power(m - i, n - i * s)))));
    for (int i = 0; i <= m; ++i)
        f[i] = times((i % 2 ? (P - 1) : 1), ifac[i]);
    std::reverse(g.begin(), g.end());
    f = conv(f, g, m + 1);
    std::reverse(f.begin(), f.end());
    int ans = 0;
    for (int i = 0; i <= m; ++i) {
        f[i] = times(ifac[i], f[i]);
        ans = plus(ans, times(f[i], val[i]));
    }
    std::cout << ans << '\n';
    
    return 0;
}