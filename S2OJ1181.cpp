#include <bits/stdc++.h>

constexpr int P = 1000000007;

int norm(const int x) {
    return x >= P ? (x - P) : x;
}

struct Info {
    int val;
    int cnt;
    Info(int x = -1, int y = 0) : val(x), cnt(y) {}
    friend Info operator +(const Info &x, const Info &y) {
        if (x.val > y.val) return x;
        if (x.val < y.val) return y;
        return Info{x.val, norm(x.cnt + y.cnt)};
    }
};

struct SegmentTree {
    std::vector<Info> s;
    SegmentTree(int n) : s(n * 4) {}
    
    void update(int u, int l, int r, int p, const Info &k) {
        if (l == r - 1) {
            s[u] = k;
            return;
        }
        int mid = (l + r) / 2;
        if (p < mid) update(u * 2, l, mid, p, k);
        else update(u * 2 + 1, mid, r, p, k);
        s[u] = s[u * 2] + s[u * 2 + 1];
    }
    
    Info range_sum(int u, int l, int r, int L, int R) {
        if (L <= l && r <= R) {
            return s[u];
        }
        int mid = (l + r) / 2;
        Info res;
        if (L < mid) res = res + range_sum(u * 2, l, mid, L, R);
        if (R > mid) res = res + range_sum(u * 2 + 1, mid, r, L, R);
        return res;
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    std::vector<int> d(n + 1), u(n + 1);
    for (int i = 1; i <= n; ++i) {
        std::cin >> d[i] >> u[i];
        d[i] = std::min(d[i], n + 1);
        u[i] = std::min(u[i], n + 1);
    }

    std::vector<Info> f(n + 1);
    f[0] = Info(0, 1);

    std::vector<int> sd(n + 1), su(n + 1);
    SegmentTree tr(n);

    auto cdq = [&](auto &&self, int l, int r) -> void {
        if (l == r - 1) {
            if (d[l] <= 1 && 1 <= u[l]) {
                f[l] = f[l] + f[l - 1];
            }
            if (~f[l].val) ++f[l].val;
            return;
        }
        int mid = (l + r) / 2;
        self(self, l, mid);
        
        std::copy(d.begin() + l, d.begin() + r, sd.begin() + l);
        std::copy(u.begin() + l, u.begin() + r, su.begin() + l);
        for (int i = mid - 2; i >= l; --i) {
            sd[i] = std::max(sd[i], sd[i + 1]);
            su[i] = std::min(su[i], su[i + 1]);
        }
        for (int i = mid + 1; i < r; ++i) {
            sd[i] = std::max(sd[i], sd[i - 1]);
            su[i] = std::min(su[i], su[i - 1]);
        }
        static std::vector<std::pair<int, int>> ord;
        for (int i = l; i < mid; ++i) {
            if (sd[i] <= su[i]) {
                ord.emplace_back(i - 1 + sd[i], i);
                ord.emplace_back(i - 1 + su[i] + 1, -i);
            }
        }
        std::sort(ord.begin(), ord.end());
        int p = 0;
        for (int i = mid; i < r; ++i) {
            while (p < int(ord.size()) && ord[p].first <= i) {
                int j = std::abs(ord[p].second) - 1;
                tr.update(1, 0, n, j, ord[p].second > 0 ? f[j] : Info());
                ++p;
            }
            int ql = std::max(i - su[i], 0);
            int qr = std::min(i - sd[i], n - 1);
            if (ql <= qr) f[i] = f[i] + tr.range_sum(1, 0, n, ql, qr + 1);
        }
        while (p < int(ord.size())) {
            if (ord[p].second < 0) {
                int j = std::abs(ord[p].second) - 1;
                tr.update(1, 0, n, j, Info());
            }
            ++p;
        }
        ord.clear();

        self(self, mid, r);
    };
    cdq(cdq, 1, n + 1);

    std::cout << f[n].val;
    if (~f[n].val) std::cout << ' ' << f[n].cnt;
    std::cout << '\n';

    return 0;
}
