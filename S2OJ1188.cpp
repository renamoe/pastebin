#include <bits/stdc++.h>

constexpr int Inf = std::numeric_limits<int>::max() / 3;

constexpr int N = 100;
constexpr int M = 1000;

int char_to_int(char c) {
    switch (c) {
        case 'A':
            return 0;
        case 'T':
            return 1;
        case 'G':
            return 2;
        case 'C':
            return 3;
        default:
            return -1;
    }
}

void check_max(int &x, const int y) {
    if (x < y) x = y;
}

namespace Trie {
    struct Node {
        int ch[4];
        int val;
        Node() : ch{-1, -1, -1, -1}, val(-Inf) {}
    };

    int tot = 1;
    Node p[M + 5];

    void insert(const std::string &s, int k) {
        int u = 0;
        for (char _c : s) {
            int c = char_to_int(_c);
            if (p[u].ch[c] == -1) {
                p[u].ch[c] = tot++;
            }
            u = p[u].ch[c];
        }
        p[u].val = std::max(p[u].val, k);
    }
}

int f[N][N + 1][M + 5];
int g[N][N + 1];
int h[N + 1];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int cut_cost;
    int del_cost;
    std::cin >> cut_cost >> del_cost;
    std::string s;
    std::cin >> s;
    int n = s.length();
    
    std::string t;
    int val;
    while (std::cin >> t >> val) {
        Trie::insert(t, val);
    }

    for (char i : {'A', 'T', 'G', 'C'}) {
        Trie::insert(std::string() + i, del_cost);
    }

    for (int l = 0; l < n; ++l) {
        for (int r = 0; r <= n; ++r) {
            std::fill_n(f[l][r], Trie::tot, -Inf);
            g[l][r] = -Inf;
        }
    }
    for (int i = 0; i < n; ++i) {
        f[i][i][0] = 0;
        g[i][i] = 0;
    }
    for (int len = 1; len <= n; ++len) {
        for (int l = 0, r = l + len; r <= n; ++l, ++r) {
            int c = char_to_int(s[r - 1]);
            for (int i = l; i < r; ++i) {
                for (int j = 0; j < Trie::tot; ++j) {
                    if (f[l][i][j] != -Inf && ~Trie::p[j].ch[c] && g[i][r - 1] != -Inf) {
                        check_max(f[l][r][Trie::p[j].ch[c]], f[l][i][j] + g[i][r - 1]);
                    }
                }
            }
            for (int j = 0; j < Trie::tot; ++j) {
                if (Trie::p[j].val != -Inf && f[l][r][j] != -Inf) {
                    check_max(g[l][r], f[l][r][j] - cut_cost + Trie::p[j].val);
                    check_max(f[l][r][0], f[l][r][j] + Trie::p[j].val);
                }
            }
            // std::cerr << s.substr(l, len) << ' ' << g[l][r] << '\n';
        }
    }
    for (int i = 1; i <= n; ++i) {
        h[i] = -Inf;
        for (int j = 0; j < i; ++j) {
            check_max(h[i], h[j] + std::max(g[j][i], 0));
        }
    }
    std::cout << h[n] << '\n';

    return 0;
}