#include <bits/stdc++.h>

constexpr int Inf = std::numeric_limits<int>::max();

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    std::vector<int> x(n), y(n), r(n);
    for (int i = 0; i < n; ++i) std::cin >> x[i] >> y[i] >> r[i];
    double ans = 0;
    for (int s = 1; s < (1 << n); ++s) {
        int maxx = -Inf;
        int maxy = -Inf;
        int minz = Inf;
        for (int i = 0; i < n; ++i) {
            if ((s >> i) & 1) {
                maxx = std::max(maxx, x[i]);
                maxy = std::max(maxy, y[i]);
                minz = std::min(minz, x[i] + y[i] + r[i]);
            }
        }
        int R = std::max(0, minz - maxx - maxy);
        ans += (double) R * R / 2.0 * std::pow(-2.0, __builtin_popcount(s) - 1);
    }
    std::cout << std::fixed << std::setprecision(1) << ans << '\n';

    return 0;
}