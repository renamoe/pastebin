#include <bits/stdc++.h>

constexpr int P = 998244353;

void inc(int &x, const int y) {
    x += y;
    if (x >= P) x -= P;
}

void solve() {
    int n, m;
    std::cin >> n >> m;
    std::vector<std::vector<int>> e(n);
    std::vector<int> deg(n);
    for (int i = 0; i < m; ++i) {
        int x, y;
        std::cin >> x >> y;
        --x;
        --y;
        e[y].push_back(x);
        ++deg[x];
    }

    std::queue<int> q;
    for (int i = 0; i < n; ++i)
        if (!deg[i]) q.push(i);
    int cnt = 0;
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        ++cnt;
        for (int v : e[u]) {
            --deg[v];
            if (!deg[v]) q.push(v);
        }
    }

    if (cnt < n) {
        std::cout << 0 << '\n';
        return;
    }

    std::vector<std::vector<int>> no_le(n, std::vector<int>(n));
    for (int i = 0; i < n; ++i) {
        for (int j : e[i]) {
            ++no_le[i][j];
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            if (i > 0) no_le[i][j] += no_le[i - 1][j];
            if (j > 0) no_le[i][j] += no_le[i][j - 1];
            if (i > 0 && j > 0) no_le[i][j] -= no_le[i - 1][j - 1];
        }
    }
    auto check = [&](int l1, int r1, int l2, int r2) -> bool {
        int sum = no_le[r1][r2];
        if (l1 > 0) sum -= no_le[l1 - 1][r2];
        if (l2 > 0) sum -= no_le[r1][l2 - 1];
        if (l1 > 0 && l2 > 0) sum += no_le[l1 - 1][l2 - 1];
        return sum == 0;
    };

    std::vector<std::vector<int>> f(n, std::vector<int>(n));
    std::vector<std::vector<int>> g(n, std::vector<int>(n));
    for (int len = 0; len < n; ++len) {
        for (int l = 0, r = l + len; r < n; ++l, ++r) {
            if (l == r) {
                f[l][r] = 1;
                g[l][r] = 1;
            } else {
                if (check(l + 1, r, l, l)) {
                    inc(f[l][r], f[l + 1][r]);
                    inc(g[l][r], f[l + 1][r]);
                }
                for (int i = l; i < r; ++i) {
                    if (check(l, i, i + 1, r)) {
                        inc(f[l][r], 1ll * g[l][i] * f[i + 1][r] % P);
                    }
                }
            }
        }
    }
    std::cout << f[0][n - 1] << '\n';
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int t;
    std::cin >> t;
    while (t--) solve();

    return 0;
}