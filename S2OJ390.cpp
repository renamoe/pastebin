#include <queue>
#include <vector>
#include <iostream>
#include <algorithm>

using u32 = unsigned int;

const int MaxN = 1000001;

struct Graph {
    int tot;
    int head[MaxN];
    int nxt[MaxN], to[MaxN];
    Graph() : tot(0) {}
    void add(int u, int v) {
        int i = ++tot;
        nxt[i] = head[u];
        to[i] = v;
        head[u] = i;
    }
} G;

int lg[37];
u32 hi_trans[65537][33], lo_trans[65537][33];

int n, m;
int a[MaxN], deg[MaxN];
int lst[MaxN], que[MaxN];
u32 f[MaxN][33];
u32 tmp[32];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    for (int i = 0; i < 32; ++i)
        lg[(1u << i) % 37] = i;

    // 2 ^ 5 = 32
    // 2 ^ 16 = 65536
    // 2 ^ 32 = 4294967296
    for (int i = 0; i < 65536; ++i) {
        for (int j = 0; j < 32; ++j) {
            for (int s = i; s; s ^= s & -s) {
                int k = lg[(s & -s) % 37];
                hi_trans[i][j] |= 1u << ((k | 16) ^ j);
                lo_trans[i][j] |= 1u << (k ^ j);
            }
        }
    }

    std::cin >> n >> m;
    for (int i = 0; i < n; ++i)
        std::cin >> a[i];
    for (int i = 0; i < m; ++i) {
        int u, v;
        std::cin >> u >> v;
        --u, --v;
        G.add(u, v);
        ++deg[v];
    }

    int l = 0, r = 0;
    for (int i = 0; i < n; ++i)
        if (!deg[i])
            que[r++] = i;
    int cnt_lst = 0;
    while (l < r) {
        int u = que[l++];
        lst[cnt_lst++] = u;
        for (int i = G.head[u]; i; i = G.nxt[i]) {
            int v = G.to[i];
            --deg[v];
            if (!deg[v])
                que[r++] = v;
        }
    }

    f[0][a[0] >> 5] = 1u << (a[0] & 31);
    for (int t = 0; t < n; ++t) {
        int u = lst[t];
        for (int i = 0; i < 32; ++i) {
            tmp[i ^ (a[u] >> 5)] = 
                    hi_trans[f[u][i] >> 16][a[u] & 31] | lo_trans[f[u][i] & 65535][a[u] & 31];
        }
        for (int i = 0; i < 32; ++i)
            f[u][i] = tmp[i];
        for (int i = G.head[u]; i; i = G.nxt[i]) {
            int v = G.to[i];
            for (int i = 0; i < 32; ++i)
                f[v][i] |= f[u][i];
        }
    }
    for (int i = 0; i < 32; ++i)
        if (f[n - 1][i]) {
            u32 s = f[n - 1][i];
            int ans = (i << 5) | lg[(s & -s) % 37];
            std::cout << ans << '\n';
            break;
        }

    return 0;
}