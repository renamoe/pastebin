#include <bits/stdc++.h>

using u64 = unsigned long long;

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    n = 1 << n;
    std::vector<int> a(n);
    for (int i = 0; i < n; ++i) std::cin >> a[i];

    if (n == 2) {
        std::cout << 2 << '\n';
        return 0;
    }

    std::mt19937_64 rnd(std::chrono::steady_clock::now().time_since_epoch().count());
    std::vector<u64> f(n);
    for (int i = 0; i < n / 2; ++i) f[i] = f[i ^ (n - 1)] = rnd();

    u64 sum = 0;
    long long ans = 1ll * n * (n + 1) / 2;
    std::unordered_map<u64, int> map[4];
    for (int i = 0; i < 4; ++i) map[i].rehash((n + 4) / 4);
    ++map[3][0];
    for (int i = 0; i < n; ++i) {
        sum ^= f[a[i]];
        ans -= map[i % 4][sum];
        ++map[i % 4][sum];
    }
    std::cout << ans << '\n';

    return 0;
}
