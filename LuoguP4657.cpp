#include <bits/stdc++.h>

using i64 = long long;

constexpr int N = 100000, M = 100;

int n, m;
i64 a[N];
std::vector<int> e[N];

i64 s[N];
i64 f[N][M + 1];
i64 g[N][M + 1];
i64 tmp[M + 1];

i64 ans;

void dp(int u, int fa) {
    i64 fv = ~fa ? a[fa] : 0ll;
    for (int i = 1; i <= m; ++i) {
        f[u][i] = std::max(f[u][i], s[u]);
        g[u][i] = std::max(g[u][i], s[u] - fv);
    }
    for (int v : e[u]) {
        if (v != fa) {
            dp(v, u);
            ans = std::max(ans, g[v][m - 1] + s[u]);
            for (int i = 0; i <= m; ++i) {
                f[u][i] = std::max(f[u][i], f[v][i]);
                if (i > 0) {
                    f[u][i] = std::max(f[u][i], f[v][i - 1] + s[u] - a[v]);
                }
            }
            for (int i = 0; i <= m; ++i) {
                g[u][i] = std::max(g[u][i], g[v][i]);
                if (i > 0) {
                    g[u][i] = std::max(g[u][i], g[v][i - 1] + s[u] - fv);
                }
            }
        }
    }
    for (int i = 1; i <= m; ++i) tmp[i] = s[u];
    for (int v : e[u]) {
        if (v != fa) {
            for (int i = 1; i <= m; ++i) {
                ans = std::max(ans, tmp[i] + g[v][m - i]);
            }
            for (int i = 1; i <= m; ++i) {
                tmp[i] = std::max(tmp[i], f[v][i]);
                tmp[i] = std::max(tmp[i], f[v][i - 1] + s[u] - a[v]);
            }
        }
    }
    std::reverse(e[u].begin(), e[u].end());
    for (int i = 1; i <= m; ++i) tmp[i] = s[u];
    for (int v : e[u]) {
        if (v != fa) {
            for (int i = 1; i <= m; ++i) {
                ans = std::max(ans, tmp[i] + g[v][m - i]);
            }
            for (int i = 1; i <= m; ++i) {
                tmp[i] = std::max(tmp[i], f[v][i]);
                tmp[i] = std::max(tmp[i], f[v][i - 1] + s[u] - a[v]);
            }
        }
    }
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::cin >> n >> m;
    for (int i = 0; i < n; ++i) std::cin >> a[i];
    for (int i = 0; i < n - 1; ++i) {
        int u, v;
        std::cin >> u >> v;
        --u;
        --v;
        e[u].push_back(v);
        e[v].push_back(u);
    }
    for (int i = 0; i < n; ++i)
        for (int j : e[i]) s[i] += a[j];
    
    if (m) dp(0, -1); // m = 0 !
    std::cout << ans << '\n';

    return 0;
}