#include <bits/stdc++.h>

constexpr int P = 1e9 + 7, inv2 = (P + 1) / 2;

void inc(int &x, const int y) {
    x += y;
    if (x >= P) x -= P;
}

int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = 1ll * r * a % P;
        a = 1ll * a * a % P;
        b >>= 1;
    }
    return r;
}

struct Matrix {
    int a[7][7];
    Matrix() : a{} {}
    Matrix(const std::initializer_list<int> &l) {
        int *p = a[0];
        for (auto i : l) *p++ = i;
    }
    int *operator [](const int k) { return a[k]; }
    const int *operator [](const int k) const { return a[k]; }
    friend Matrix operator *(const Matrix &x, const Matrix &y) {
        Matrix r;
        for (int i = 0; i < 7; ++i)
            for (int j = 0; j < 7; ++j)
                for (int k = 0; k < 7; ++k)
                    r[i][j] = (r[i][j] + 1ll * x[i][k] * y[k][j]) % P;
        return r;
    }
    Matrix pow(int y) const {
        Matrix r;
        for (int i = 0; i < 7; ++i) r[i][i] = 1;
        Matrix x = *this;
        while (y) {
            if (y & 1) r = r * x;
            x = x * x;
            y >>= 1;
        }
        return r;
    }
};

struct BIT {
    int n;
    std::vector<int> s;
    BIT(int n) : n(n), s(n + 1) {}
    void add(int x, int k) {
        for (int i = x; i <= n; i += i & -i)
            inc(s[i], k);
    }
    int query(int x) {
        int res = 0;
        for (int i = x; i; i -= i & -i)
            inc(res, s[i]);
        return res;
    }
};

constexpr int N = 500000;

int a[N + 1];

int C2(int x) {
    return (1ll * x * (x - 1) / 2) % P;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, k;
    std::cin >> n >> k;
    for (int i = 1; i <= n; ++i) std::cin >> a[i];

    if (n == 1) {
        std::cout << 0 << '\n';
        return 0;
    }
    if (n == 2) {
        std::cout << (int(a[1] > a[2]) ^ (k & 1)) << '\n';
        return 0;
    }

    Matrix trans {
        C2(n - 2), 1, n - 2, 0, 0, n - 2, 0,
        1, C2(n - 2), 0, n - 2, n - 2, 0, 0,
        1, 0, (C2(n - 2) + n - 3) % P, 1, 1, 0, n - 3,
        0, 1, 1, (C2(n - 2) + n - 3) % P, 0, 1, n - 3,
        0, 1, 1, 0, (C2(n - 2) + n - 3) % P, 1, n - 3,
        1, 0, 0, 1, 1, (C2(n - 2) + n - 3) % P, n - 3,
        0, 0, 1, 1, 1, 1, (C2(n - 2) + 2 * (n - 4) + 1) % P
    };

    Matrix A;
    A[0][0] = 1;
    A = A * trans.pow(k);

    int ans = 0;
    const auto &f = A[0];
    BIT tr0(n);
    BIT tr1(n);
    int inv = power(n - 2, P - 2);
    for (int i = 1; i <= n; ++i) {
        int ca = tr0.query(a[i]);
        int cb = i - 1 - ca;
        int sa = tr1.query(a[i]);
        int sb = ((1ll * (i - 1) * (i - 2) / 2) % P - sa + P) % P;
        int ta = (1ll * (n - 2) * ca % P - sa + P) % P;
        int tb = (1ll * (n - 2) * cb % P - sb + P) % P;

        // (A, B)
        inc(ans, 1ll * f[0] * cb % P);
        // (B, A)
        inc(ans, 1ll * f[1] * ca % P);
        // (A, C)
        inc(ans, 1ll * f[2] * ((tb + sa) % P) % P * inv % P);
        // (C, A)
        inc(ans, 1ll * f[3] * ((ta + sb) % P) % P * inv % P);
        // (B, C)
        inc(ans, 1ll * f[4] * ((1ll * ca * (i - 2) + 1ll * cb * (n - i)) % P) % P * inv % P);
        // (C, B)
        inc(ans, 1ll * f[5] * ((1ll * cb * (i - 2) + 1ll * ca * (n - i)) % P) % P * inv % P);
        // (C, C)
        inc(ans, 1ll * f[6] * (i - 1) % P * inv2 % P);

        tr0.add(a[i], 1);
        tr1.add(a[i], i - 1);
    }
    std::cout << ans << '\n';

    return 0;
}