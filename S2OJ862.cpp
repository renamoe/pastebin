#include <bits/stdc++.h>

constexpr int P = 1e9 + 7;

int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = 1ll * r * a % P;
        a = 1ll * a * a % P;
        b >>= 1;
    }
    return r;
}
int inverse(int x) { return power(x, P - 2); }

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    std::vector<int> A(n), B(n);
    for (int i = 0; i < n; ++i) std::cin >> A[i];
    for (int i = 0; i < n; ++i) std::cin >> B[i];

    std::vector<int> fac(n + 1), ifac(n + 1);
    fac[0] = ifac[0] = 1;
    for (int i = 1; i <= n; ++i) fac[i] = 1ll * fac[i - 1] * i % P;
    ifac[n] = inverse(fac[n]);
    for (int i = n; i > 1; --i) ifac[i - 1] = 1ll * ifac[i] * i % P;
    auto comb = [&](int x, int y) -> int {
        if (x < 0 || y < 0 || x < y) return 0;
        return 1ll * fac[x] * ifac[y] % P * ifac[x - y] % P;
    };

    std::vector<int> t(A);
    t.insert(t.end(), B.begin(), B.end());
    std::sort(t.begin(), t.end());
    t.erase(std::unique(t.begin(), t.end()), t.end());

    std::sort(A.begin(), A.end());
    std::sort(B.begin(), B.end());

    std::reverse(t.begin(), t.end());
    int ans = 1;
    int x = n;
    int y = n;
    int X = n;
    int Y = n;
    for (auto k : t) {
        while (x && A[x - 1] >= k) --x;
        while (y && B[y - 1] >= k) --y;
        
        int a = X - x;
        int b = Y - y;
        int c = n - X;
        int d = n - Y;
        int sum = 0;
        for (int i = 0; i <= a; ++i) {
            int now = 1ll * comb(a, i) * power(1ll * power(k, i) * (power(k + 1, a + c - i) - power(k, a + c - i) + P) % P, b) % P * power(1ll * power(k, i) * power(k + 1, a - i) % P, d) % P;
            sum = (sum + 1ll * (i % 2 == 1 ? (P - 1) : 1) * now) % P;
        }
        ans = 1ll * ans * sum % P;

        X = x;
        Y = y;
    }
    std::cout << ans << '\n';    
    
    return 0;
}