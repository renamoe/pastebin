#include <bits/stdc++.h>

using i64 = long long;

struct SegmentTree {
    struct Node {
        Node *ls, *rs;
        int cnt;
        i64 val;
        Node(int c = 0) : ls{}, rs{}, cnt(c), val(0) {}
    };

    Node *root;
    int lim;
    int det;
    int end_pos;
    i64 base;
    SegmentTree() {}
    SegmentTree(int l, int n, int d, i64 b)
        : lim(l), det(d), end_pos(n), base(b) {
        root = new Node(n);
    }

    i64 erase_kth(Node *u, int l, int r, int k) {
        --u->cnt;
        if (l == r - 1) {
            if (u->val) {
                return u->val;
            } else {
                return base + 1ll * l * det;
            }
        }
        int mid = (l + r) / 2;
        int lsize = u->ls ? u->ls->cnt : std::min(mid - l, u->cnt + 1);
        if (k <= lsize) {
            if (!u->ls) u->ls = new Node(lsize);
            return erase_kth(u->ls, l, mid, k);
        }
        if (!u->rs) u->rs = new Node(u->cnt + 1 - lsize); // +1
        return erase_kth(u->rs, mid, r, k - lsize);
    }
    i64 erase_kth(int k) {
        return erase_kth(root, 1, lim, k);
    }

    void insert(Node *u, int l, int r, int p, i64 v) {
        ++u->cnt;
        if (l == r - 1) {
            u->val = v;
            return;
        }
        int mid = (l + r) / 2;
        int lsize = u->ls ? u->ls->cnt : std::min(mid - l, u->cnt - 1);
        if (p < mid) {
            if (!u->ls) u->ls = new Node(lsize);
            insert(u->ls, l, mid, p, v);
        } else {
            if (!u->rs) u->rs = new Node(u->cnt - 1 - lsize); // -1
            insert(u->rs, mid, r, p, v);
        }
    }
    void push_back(i64 v) {
        ++end_pos;
        insert(root, 1, lim, end_pos, v);
    }
};

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, m, Q;
    std::cin >> n >> m >> Q;
    std::vector<SegmentTree> tr(n + 1);
    for (int i = 1; i <= n; ++i) 
        tr[i] = SegmentTree(m + Q + 1, m - 1, 1, 1ll * (i - 1) * m);
    tr[0] = SegmentTree(n + Q + 1, n, m, 0);

    while (Q--) {
        int x, y;
        std::cin >> x >> y;
        i64 tar = tr[0].erase_kth(x);
        if (y < m) {
            tr[x].push_back(tar);
            tar = tr[x].erase_kth(y);
        }
        tr[0].push_back(tar);
        std::cout << tar << '\n';
    }

    return 0;
}