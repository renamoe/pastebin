#include <bits/stdc++.h>

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int T;
    std::cin >> T;
    while (T--) {
        int H, W;
        std::cin >> H >> W;
        int n = H + W;
        std::vector<std::vector<int>> e0(n), e1(n);
        std::string str;
        for (int i = 0; i < H; ++i) {
            std::cin >> str;
            for (int j = 0; j < W; ++j) {
                if (str[j] == 'o') {
                    e0[i].push_back(H + j);
                    e0[H + j].push_back(i);
                } else if (str[j] == 'x') {
                    e1[i].push_back(H + j);
                    e1[H + j].push_back(i);
                }
            }
        }
        bool flag = true;
        int cnt[2];
        std::vector<int> vis(n, -1);
        std::function<void(int, int)> dfs = [&](int u, int c) {
            vis[u] = c;
            cnt[c] ^= 1;
            for (int v : e0[u]) {
                if (vis[v] == -1) {
                    dfs(v, c);
                } else if (vis[v] != c) {
                    flag = false;
                }
            }
            for (int v : e1[u]) {
                if (vis[v] == -1) {
                    dfs(v, c ^ 1);
                } else if (vis[v] == c) {
                    flag = false;
                }
            }
        };
        int f[3] = {};
        for (int i = 0; i < n; ++i) {
            if (vis[i] == -1) {
                cnt[0] = cnt[1] = 0;
                dfs(i, 0);
                f[cnt[0] + cnt[1]] ^= 1;
            }
        }
        if (flag) {
            int sg = (f[0] * 0) ^ (f[1] * 2) ^ (f[2] * 1);
            if (sg) {
                std::cout << 3 << '\n';
            } else {
                std::cout << 2 << '\n';
            }
        } else {
            std::cout << (n % 2) << '\n';
        }
    }

    return 0;
}