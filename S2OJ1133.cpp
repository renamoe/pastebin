#include <bits/stdc++.h>

using u32 = unsigned int;
using u64 = unsigned long long;

constexpr int N = 500;

u32 P;

void inc(u32 &x, const u32 y) {
    x += y;
    if (x >= P) x -= P;
}

u32 f[N + 1][N];
u32 g[N + 1][N];
u32 h[N + 1][N];

u32 binom[N + 1][N + 1];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n >> P;

    for (int i = 0; i <= n; ++i) {
        binom[i][0] = 1;
        for (int j = 1; j <= i; ++j) {
            inc(binom[i][j], binom[i - 1][j]);
            inc(binom[i][j], binom[i - 1][j - 1]);
        }
    }

    f[1][0] = 1;
    for (int i = 0; i <= n; ++i) g[i][0] = 1;
    for (int d = 1; d < n; ++d) {
        g[0][d] = 1;
        for (int i = 1; i <= n; ++i) {
            f[i][d] = u64(i) * g[i - 1][d - 1] % P;
            for (int j = 1; j <= i; ++j) {
                inc(g[i][d], u64(g[i - j][d]) * f[j][d] % P * binom[i - 1][j - 1] % P);
            }
        }
    }

    h[1][0] = 1;
    for (int d = 1; d < n; ++d) {
        for (int i = 1; i <= n; ++i) {
            h[i][d] = f[i][d];
            inc(h[i][d], P - f[i][d - 1]);
        }
    }

    u32 ans = 0;
    for (int d = 2; d < n; d += 2) {
        u32 sum = h[n][d / 2];
        for (int i = 1; i < n; ++i) {
            inc(sum, P - u64(h[i][d / 2 - 1]) * f[n - i][d / 2 - 1] % P * binom[n][i] % P);
        }
        inc(ans, u64(d) * sum % P);
    }

    for (int d = 1; d < n; d += 2) {
        u32 sum = 0;
        for (int i = 1; i < n; ++i) {
            inc(sum, u64(h[i][d / 2]) * h[n - i][d / 2] % P * binom[n - 1][i - 1] % P);
        }
        inc(ans, u64(d) * sum % P);
    }

    std::cout << ans << '\n';

    return 0;
}
