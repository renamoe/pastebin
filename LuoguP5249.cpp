#include <bits/stdc++.h>

using f64 = double;

constexpr int N = 10000;

f64 f[2][N];
f64 k[N], b[N];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    double p;
    int n, s;
    std::cin >> p >> n >> s;
    --s;

    if (p == 0.0) {
        std::cout << "0.00000000" << '\n';
        return 0;
    }

    int cur = 0;
    f[0][0] = 1;
    for (int i = 1; i <= n - 1; ++i) {
        k[0] = 1;
        b[0] = 0;
        for (int j = 1; j < i + 1; ++j) {
            k[j] = (1 - p) * k[j - 1];
            b[j] = (1 - p) * b[j - 1] + p * f[cur][j - 1];
        }
        f64 sum_k = std::accumulate(k, k + i + 1, 0.0);
        f64 sum_b = std::accumulate(b, b + i + 1, 0.0);
        f[cur ^ 1][0] = (1 - sum_b) / sum_k;
        for (int j = 1; j < i + 1; ++j) {
            f[cur ^ 1][j] = f[cur ^ 1][0] * k[j] + b[j];
        }
        cur ^= 1;
    }
    std::cout << std::fixed << std::setprecision(10) << f[cur][s] << '\n';

    return 0;
}