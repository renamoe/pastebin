#include <deque>
#include <vector>
#include <iostream>
#include <algorithm>
#include <functional>

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int cases;
    std::cin >> cases;
    while (cases--) {
        int n, m;
        std::cin >> n >> m;
        std::vector<int> w(n), c(n), d(n);
        for (int i = 0; i < n; ++i) std::cin >> w[i];
        for (int i = 0; i < n; ++i) std::cin >> c[i];
        for (int i = 0; i < n; ++i) std::cin >> d[i];
        std::vector<std::vector<int>> e(n);
        for (int i = 0; i < n - 1; ++i) {
            int u, v;
            std::cin >> u >> v;
            --u;
            --v;
            e[u].push_back(v);
            e[v].push_back(u);
        }
        std::vector<int> size(n), maxs(n), vis(n);
        int rt = -1;
        int all = n;
        std::function<void(int, int)> find_rt = [&](int u, int fa) {
            size[u] = 1;
            maxs[u] = 0;
            for (int v : e[u]) {
                if (v != fa && !vis[v]) {
                    find_rt(v, u);
                    size[u] += size[v];
                    maxs[u] = std::max(maxs[u], size[v]);
                }
            }
            maxs[u] = std::max(maxs[u], all - size[u]);
            if (rt == -1 || maxs[u] < maxs[rt])
                rt = u;
        };
        int ans = 0;
        std::vector<int> lst, end(n);
        std::function<void(int, int)> dfs = [&](int u, int fa) {
            lst.push_back(u);
            for (int v : e[u])
                if (v != fa && !vis[v])
                    dfs(v, u);
            end[u] = lst.size();
        };
        std::function<void(int)> solve = [&](int u) {
            lst.clear();
            dfs(u, -1);
            std::vector<std::vector<int>> f(lst.size() + 1, std::vector<int>(m + 1));
            std::deque<std::pair<int, int>> q;
            for (int i = int(lst.size() - 1); ~i; --i) {
                int v = lst[i];
                f[i] = f[end[v]];
                for (int j = 0; j < c[v]; ++j) {
                    q.clear();
                    for (int k = 0; k * c[v] + j <= m; ++k) {
                        while (!q.empty() && q.front().first < k - d[v])
                            q.pop_front();
                        if (!q.empty())
                            f[i][k * c[v] + j] = std::max(f[i][k * c[v] + j], q.front().second + k * w[v]);
                        int val = f[i + 1][k * c[v] + j] - k * w[v];
                        while (!q.empty() && q.back().second <= val)
                            q.pop_back();
                        q.emplace_back(k, val);
                    }
                }
            }
            for (int i = 0; i <= m; ++i)
                ans = std::max(ans, f[0][i]);
            vis[u] = true;
            int now = all;
            for (int v : e[u]) {
                if (!vis[v]) {
                    all = size[v] > size[u] ? (now - size[v]) : size[v];
                    rt = -1;
                    find_rt(v, u);
                    solve(rt);
                }
            }
        };
        find_rt(0, -1);
        solve(rt);
        std::cout << ans << '\n';
    }
    
    return 0;
}