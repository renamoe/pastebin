#include <bits/stdc++.h>

const int P = 998244353, R = 3;

int plus(const int x, const int y) {
    return (x + y >= P) ? (x + y - P) : (x + y);
}
int times(const int x, const int y) {
    return (long long) x * y % P;
}
int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = times(r, a);
        a = times(a, a);
        b >>= 1;
    }
    return r;
}

void dft(std::vector<int> &a) {
    static std::vector<int> rev, roots{0, 1};
    int n = a.size();
    if (int(rev.size()) != n) {
        int k = __builtin_ctz(n) - 1;
        rev.resize(n);
        for (int i = 0; i < n; ++i)
            rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << k);
    }
    for (int i = 0; i < n; ++i)
        if (rev[i] < i) std::swap(a[rev[i]], a[i]);
    if (int(roots.size()) < n) {
        int k = __builtin_ctz(roots.size());
        roots.resize(n);
        while ((1 << k) < n) {
            int wn = power(R, (P - 1) >> (k + 1));
            for (int i = 1 << (k - 1); i < (1 << k); ++i) {
                roots[i * 2] = roots[i];
                roots[i * 2 + 1] = times(roots[i], wn);
            }
            ++k;
        }
    }
    for (int k = 1; k < n; k *= 2) {
        for (int i = 0; i < n; i += k * 2) {
            for (int j = 0; j < k; ++j) {
                int x = a[i + j];
                int y = times(a[i + k + j], roots[k + j]);
                a[i + j] = plus(x, y);
                a[i + k + j] = plus(x, P - y);
            }
        }
    }
}

void idft(std::vector<int> &a) {
    int n = a.size();
    std::reverse(a.begin() + 1, a.end());
    dft(a);
    int invn = power(n, P - 2);
    for (int i = 0; i < n; ++i) a[i] = times(a[i], invn);
}

std::vector<int> conv(std::vector<int> a, std::vector<int> b) {
    int tot = a.size() + b.size() - 1;
    int n = 1;
    while (n < tot) n *= 2;
    std::vector<int> res(n);
    a.resize(n);
    b.resize(n);
    dft(a);
    dft(b);
    for (int i = 0; i < n; ++i) res[i] = times(a[i], b[i]);
    idft(res);
    return res;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, m;
    std::cin >> m >> n;
    std::string s, t;
    std::cin >> s >> t;
    std::reverse(s.begin(), s.end());

    std::vector<int> f(m), g(n), sum(n + m - 1);
    for (int i = 0; i < m; ++i) {
        if (s[i] != '*') f[i] = int(s[i] - 'a' + 1);
        f[i] = f[i] * f[i] * f[i];
    }
    for (int i = 0; i < n; ++i) {
        if (t[i] != '*') g[i] = int(t[i] - 'a' + 1);
    }
    auto res = conv(f, g);
    for (int i = 0; i < n + m - 1; ++i)
        sum[i] += res[i];

    f.assign(m, 0);
    f.assign(n, 0);
    for (int i = 0; i < m; ++i) {
        if (s[i] != '*') f[i] = int(s[i] - 'a' + 1);
        f[i] = f[i] * f[i];
    }
    for (int i = 0; i < n; ++i) {
        if (t[i] != '*') g[i] = int(t[i] - 'a' + 1);
        g[i] = g[i] * g[i];
    }
    res = conv(f, g);
    for (int i = 0; i < n + m - 1; ++i)
        sum[i] -= 2 * res[i];

    f.assign(m, 0);
    f.assign(n, 0);
    for (int i = 0; i < m; ++i) {
        if (s[i] != '*') f[i] = int(s[i] - 'a' + 1);
    }
    for (int i = 0; i < n; ++i) {
        if (t[i] != '*') g[i] = int(t[i] - 'a' + 1);
        g[i] = g[i] * g[i] * g[i];
    }
    res = conv(f, g);
    for (int i = 0; i < n + m - 1; ++i)
        sum[i] += res[i];
    
    std::vector<int> ans;
    for (int i = m - 1; i <= n - 1; ++i)
        if (sum[i] == 0) ans.push_back(i - m + 2);
    std::cout << ans.size() << '\n';
    for (int i : ans) std::cout << i << ' ';
    std::cout << '\n';
    
    return 0;
}