#include <bits/stdc++.h>

struct Complex {
    double real, imag;
    Complex(double x = 0, double y = 0) : real(x), imag(y) {}
    friend Complex operator +(const Complex &x, const Complex &y) {
        return Complex(x.real + y.real, x.imag + y.imag);
    }
    friend Complex operator -(const Complex &x, const Complex &y) {
        return Complex(x.real - y.real, x.imag - y.imag);
    }
    friend Complex operator *(const Complex &x, const Complex &y) {
        return Complex(x.real * y.real - x.imag * y.imag, x.real * y.imag + x.imag * y.real);
    }
};

constexpr double Pi = acos(-1.0);

void dft(std::vector<Complex> &a) {
    static std::vector<int> rev;
    static std::vector<Complex> roots{Complex(0, 0), Complex(1, 0)};
    int n = a.size();
    if (int(rev.size()) != n) {
        int k = __builtin_ctz(n) - 1;
        rev.resize(n);
        for (int i = 0; i < n; ++i)
            rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << k);
    }
    for (int i = 0; i < n; ++i)
        if (rev[i] < i) std::swap(a[rev[i]], a[i]);
    if (int(roots.size()) < n) {
        int k = __builtin_ctz(roots.size());
        roots.resize(n);
        while ((1 << k) < n) {
            double d = Pi / double(1 << k);
            Complex w(std::cos(d), std::sin(d));
            for (int i = 1 << (k - 1); i < (1 << k); ++i) {
                roots[i * 2] = roots[i];
                roots[i * 2 + 1] = roots[i] * w;
            }
            ++k;
        }
    }
    for (int k = 1; k < n; k *= 2) {
        for (int i = 0; i < n; i += k * 2) {
            for (int j = 0; j < k; ++j) {
                Complex x = a[i + j];
                Complex y = a[i + k + j] * roots[k + j];
                a[i + j] = x + y;
                a[i + k + j] = x - y;
            }
        }
    }
}

void idft(std::vector<Complex> &a) {
    int n = a.size();
    std::reverse(a.begin() + 1, a.end());
    dft(a);
    for (int i = 0; i < n; ++i) {
        a[i].real /= n;
        a[i].imag /= n;
    }
}

std::vector<Complex> conv(std::vector<Complex> a, std::vector<Complex> b) {
    int tot = a.size() + b.size() - 1;
    int n = 1;
    while (n < tot) n *= 2;
    a.resize(n);
    b.resize(n);
    dft(a);
    dft(b);
    std::vector<Complex> res(n);
    for (int i = 0; i < n; ++i) res[i] = a[i] * b[i];
    idft(res);
    return res;
}

const int P = 1e9 + 7;

int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = 1ll * r * a % P;
        a = 1ll * a * a % P;
        b >>= 1;
    }
    return r;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::string s0;
    std::cin >> s0;
    std::string s((s0.size() + 1) * 2, '#');
    s[0] = '@';
    for (int i = 0; i < int(s0.size()); ++i)
        s[(i + 1) * 2] = s0[i];
    int n = s.size();
    std::vector<int> ans(n);

    std::vector<Complex> f(n);
    for (int i = 0; i < n; ++i) f[i].real = s[i] == 'a';
    f = conv(f, f);
    for (int i = 0; i < n; ++i) ans[i] = (ans[i] + ((long long)(f[i * 2].real + 0.5) + 1) / 2) % (P - 1);

    f.assign(n, Complex(0, 0));
    for (int i = 0; i < n; ++i) f[i].real = s[i] == 'b';
    f = conv(f, f);
    for (int i = 0; i < n; ++i) ans[i] = (ans[i] + ((long long)(f[i * 2].real + 0.5) + 1) / 2) % (P - 1);
    
    // std::cerr << s << '\n';
    // for (int i = 0; i < n; ++i) std::cerr << i << ' ' << ans[i] << '\n';

    std::vector<int> p(n);
    int r = 0, mid = 0;
    for (int i = 1; i < n; ++i) {
        p[i] = 1;
        if (i <= r) p[i] = std::min(p[mid * 2 - i], r - i + 1);
        while (s[i + p[i]] == s[i - p[i]]) ++p[i];
        if (i + p[i] - 1 > r) {
            mid = i;
            r = i + p[i] - 1;
        }
    }
    // for (int i = 0; i < n; ++i) std::cerr << i << ' ' << p[i] / 2 << '\n';
    
    int sum = 0;
    for (int i = 0; i < n; ++i)
        sum = (sum + (power(2, ans[i]) - 1 - p[i] / 2 + P) % P) % P;
    std::cout << sum << '\n';
    
    return 0;
}