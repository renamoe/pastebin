#include <bits/stdc++.h>

using f80 = long double;

struct Poly {
    std::vector<f80> a;
    
    Poly(f80 a0 = 0) : a({a0}) {}
    Poly(const std::vector<f80> &_a) : a(_a) {}

    int size() const {
        return a.size();
    }
    const f80 operator [](const int i) const {
        if (i >= size() || i < 0) return 0;
        return a[i];
    }
    friend Poly operator +(const Poly &x, const Poly &y) {
        int n = std::max(x.size(), y.size());
        std::vector<f80> res(n);
        for (int i = 0; i < n; ++i) res[i] = x[i] + y[i];
        return res;
    }
    friend Poly operator -(const Poly &x, const Poly &y) {
        int n = std::max(x.size(), y.size());
        std::vector<f80> res(n);
        for (int i = 0; i < n; ++i) res[i] = x[i] - y[i];
        return res;
    }
    friend Poly operator *(const Poly &x, const Poly &y) {
        std::vector<f80> res(x.size() + y.size() - 1);
        for (int i = 0; i < x.size(); ++i) {
            for (int j = 0; j < y.size(); ++j) {
                res[i + j] += x[i] * y[j];
            }
        }
        return res;
    }
    Poly integr() const {
        int n = size();
        std::vector<f80> res(n + 1);
        for (int i = 1; i <= n; ++i) res[i] = a[i - 1] / i;
        return res;
    }
    f80 evaluate(f80 x) const {
        f80 res = 0;
        f80 t = 1;
        for (f80 i : a) {
            res += i * t;
            t *= x;
        }
        return res;
    }
};

int main() {
    int n, L;
    std::cin >> n >> L;
    std::vector<int> a(n), b(n);
    for (int i = 0; i < n; ++i) {
        std::cin >> a[i] >> a[i] >> b[i];
    }
    std::vector<f80> ans(n + 1);
    for (int y = 0; y < L; ++y) {
        std::vector<Poly> f(n + 1);
        f[0] = Poly({1.0});
        for (int i = 0; i < n; ++i) {
            Poly p;
            if (a[i] <= y && y - a[i] + 1 <= b[i]) {
                p = Poly({1.0 + f80(a[i]) / b[i], -1.0 / b[i]});
            } else if (a[i] > y && a[i] - y <= b[i]) {
                p = Poly({1.0 - f80(a[i]) / b[i], 1.0 / b[i]});
            } else {
                continue;
            }
            for (int j = i + 1; j >= 0; --j) {
                f[j] = f[j] * (1 - p);
                if (j > 0) f[j] = f[j] + f[j - 1] * p;
            }
        }
        for (int i = 0; i <= n; ++i) {
            f[i] = f[i].integr();
            ans[i] += f[i].evaluate(y + 1) - f[i].evaluate(y);
        }
    }
    for (int i = 0; i <= n; ++i) ans[i] /= L;
    f80 E = 0;
    for (int i = 1; i <= n; ++i) E += i * ans[i];
    std::cout << std::fixed << std::setprecision(6);
    std::cout << E << '\n';
    for (int i = 0; i <= n; ++i) std::cout << ans[i] << " \n"[i == n];

    return 0;
}
