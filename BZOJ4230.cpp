#include <bits/stdc++.h>

using i64 = int64_t;

i64 pw10[20];

int lg10(i64 n) {
    int l = 0;
    while (n) {
        n /= 10;
        ++l;
    }
    return l - 1;
}

std::pair<int, i64> dfs(int max, i64 n) {
    static std::map<i64, std::pair<int, i64>> mem[10];
    if (mem[max].count(n)) return mem[max][n];
    auto &res = mem[max][n];
    if (n == 0) {
        return res = {0, 0};
    }
    if (n <= 9) {
        if (n >= max) return res = {0, 1};
        return res = {max - n, 1};
    }
    int lg = lg10(n);
    int hi = n / pw10[lg];
    res = dfs(std::max(max, hi), n % pw10[lg]);
    for (int i = hi - 1; i >= 0; --i) {
        if (res.first == 0) {
            res.first = std::max(max, i + 1);
            res.second += 1;
        }
        auto now = dfs(std::max(max, i), pw10[lg] - res.first);
        res = {now.first, res.second + now.second};
    }
    return res;
}

int main() {
    i64 n;
    std::cin >> n;
    int lg = lg10(n);
    pw10[0] = 1;
    for (int i = 1; i <= lg; ++i) pw10[i] = pw10[i - 1] * 10;
    std::cout << dfs(0, n).second << '\n';

    return 0;
}