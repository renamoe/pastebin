#include <bits/stdc++.h>

constexpr int N = 1000;
constexpr int N2 = (N * N + N) * 4;
constexpr int M = 3000;

struct Rect {
    int x1, y1, x2, y2;
};

struct Point {
    int x, y;
};

Rect a[N];
int tot;
Point p[N2];
int id[M + 1][M + 1];
std::vector<int> e[N2];
int dis[N2];

void check(int rx1, int rx2, int ry, int cy1, int cy2, int cx) {
    if (rx1 < cx && cx < rx2 && cy1 < ry && ry < cy2) {
        p[tot++] = Point{cx, ry};
    }
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    for (int i = 0; i < n; ++i) {
        std::cin >> a[i].x1 >> a[i].y1 >> a[i].x2 >> a[i].y2;
    }
    int st = std::min_element(a, a + n, [](const Rect &lhs, const Rect &rhs) {
        return lhs.x1 < rhs.x1;
    }) - a;
    int ed = std::max_element(a, a + n, [](const Rect &lhs, const Rect &rhs) {
        return lhs.x2 < rhs.x2;
    }) - a;
    int sav1 = -1;
    int sav2 = -1;
    for (int i = 0; i < n; ++i) {
        if (i != st) {
            p[tot++] = Point{a[i].x1, a[i].y2};
        }
        if (i != ed) {
            p[tot++] = Point{a[i].x2, a[i].y1};
        }
        p[tot++] = Point{a[i].x1, a[i].y1};
        p[tot++] = Point{a[i].x2, a[i].y2};
        if (i == st) {
            sav1 = tot - 2;
            sav2 = tot - 1;
        }
    }
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < i; ++j) {
            check(a[i].x1, a[i].x2, a[i].y1, a[j].y1, a[j].y2, a[j].x1);
            check(a[i].x1, a[i].x2, a[i].y2, a[j].y1, a[j].y2, a[j].x1);
            check(a[i].x1, a[i].x2, a[i].y1, a[j].y1, a[j].y2, a[j].x2);
            check(a[i].x1, a[i].x2, a[i].y2, a[j].y1, a[j].y2, a[j].x2);
            check(a[j].x1, a[j].x2, a[j].y1, a[i].y1, a[i].y2, a[i].x1);
            check(a[j].x1, a[j].x2, a[j].y2, a[i].y1, a[i].y2, a[i].x1);
            check(a[j].x1, a[j].x2, a[j].y1, a[i].y1, a[i].y2, a[i].x2);
            check(a[j].x1, a[j].x2, a[j].y2, a[i].y1, a[i].y2, a[i].x2);
        } 
    }

    std::fill_n(id[0], sizeof(id) / sizeof(int), -1);
    for (int i = 0; i < tot; ++i) {
        id[p[i].x][p[i].y] = i;
    }
    for (int i = 1; i <= M; ++i) {
        int last = -1;
        for (int j = 1; j <= M; ++j) {
            if (~id[i][j]) {
                if (~last) {
                    e[last].push_back(id[i][j]);
                    e[id[i][j]].push_back(last);
                }
                last = id[i][j];
            }
        }
    }
    for (int j = 1; j <= M; ++j) {
        int last = -1;
        for (int i = 1; i <= M; ++i) {
            if (~id[i][j]) {
                if (~last) {
                    e[last].push_back(id[i][j]);
                    e[id[i][j]].push_back(last);
                }
                last = id[i][j];
            }
        }
    }

    int S = tot++;
    int T = tot++;

    int u = sav1;
    int pre = -1;
    while (true) {
        e[S].push_back(u);
        int nxt = -1;
        for (int v : e[u]) {
            if (v != pre && p[v].x < p[u].x && p[v].y == p[u].y) {
                nxt = v;
            }
        }
        if (nxt == -1) {
            for (int v : e[u]) {
                if (p[v].x == p[u].x && p[v].y < p[u].y) {
                    nxt = v;
                }
            }
        } 
        if (nxt == -1) {
            for (int v : e[u]) {
                if (v != pre && p[v].x > p[u].x && p[v].y == p[u].y) {
                    nxt = v;
                }
            }
        } 
        if (~nxt) {
            pre = u;
            u = nxt;
        } else {
            break;
        }
    }

    u = sav2;
    pre = -1;
    while (true) {
        e[u].push_back(T);
        int nxt = -1;
        for (int v : e[u]) {
            if (v != pre && p[v].x > p[u].x && p[v].y == p[u].y) {
                nxt = v;
            }
        }
        if (nxt == -1) {
            for (int v : e[u]) {
                if (p[v].x == p[u].x && p[v].y < p[u].y) {
                    nxt = v;
                }
            }
        } 
        if (nxt == -1) {
            for (int v : e[u]) {
                if (v != pre && p[v].x < p[u].x && p[v].y == p[u].y) {
                    nxt = v;
                }
            }
        } 
        if (~nxt) {
            pre = u;
            u = nxt;
        } else {
            break;
        }
    }

    std::queue<int> q;
    q.push(S);
    std::fill(dis, dis + tot, -1);
    dis[S] = 0;
    while (!q.empty()) {
        int u = q.front();
        q.pop();
        for (int v : e[u]) {
            if (dis[v] == -1) {
                dis[v] = dis[u] + 1;
                q.push(v);
            }
        }
    }
    std::cout << (dis[T] - 2) << '\n';

    return 0;
}
