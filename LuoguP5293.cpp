#include <bits/stdc++.h>

constexpr long double Pi = acos(-1.0);

struct Complex {
    long double real, imag;
    Complex(long double x = 0, long double y = 0) : real(x), imag(y) {}
    friend Complex operator +(const Complex &x, const Complex &y) {
        return Complex(x.real + y.real, x.imag + y.imag);
    }
    friend Complex operator -(const Complex &x, const Complex &y) {
        return Complex(x.real - y.real, x.imag - y.imag);
    }
    friend Complex operator *(const Complex &x, const Complex &y) {
        return Complex(x.real * y.real - x.imag * y.imag, x.real * y.imag + x.imag * y.real);
    }
    Complex conj() const {
        return Complex(real, -imag);
    }
};

void dft(std::vector<Complex> &a) {
    static std::vector<int> rev;
    static std::vector<Complex> roots{Complex(0, 0), Complex(1, 0)};
    int n = a.size();
    if (int(rev.size()) != n) {
        int k = __builtin_ctz(n) - 1;
        rev.resize(n);
        for (int i = 0; i < n; ++i)
            rev[i] = (rev[i >> 1] >> 1) | ((i & 1) << k);
    }
    for (int i = 0; i < n; ++i)
        if (rev[i] < i) std::swap(a[rev[i]], a[i]);
    if (int(roots.size()) < n) {
        int k = __builtin_ctz(roots.size());
        roots.resize(n);
        while ((1 << k) < n) {
            double d = Pi / double(1 << k);
            Complex w(std::cos(d), std::sin(d));
            for (int i = 1 << (k - 1); i < (1 << k); ++i) {
                roots[i * 2] = roots[i];
                roots[i * 2 + 1] = roots[i] * w;
            }
            ++k;
        }
    }
    for (int k = 1; k < n; k *= 2) {
        for (int i = 0; i < n; i += k * 2) {
            for (int j = 0; j < k; ++j) {
                Complex x = a[i + j];
                Complex y = a[i + k + j] * roots[k + j];
                a[i + j] = x + y;
                a[i + k + j] = x - y;
            }
        }
    }
}

void idft(std::vector<Complex> &a) {
    int n = a.size();
    std::reverse(a.begin() + 1, a.end());
    dft(a);
    for (int i = 0; i < n; ++i) {
        a[i].real /= n;
        a[i].imag /= n;
    }
}

std::vector<int> mtt(const std::vector<int> &f, const std::vector<int> &g, int p, int lim) {
    int tot = f.size() + g.size() - 1;
    int n = 1;
    while (n < tot) n *= 2;
    std::vector<Complex> a(n), b(n), c(n), d(n);
    for (int i = 0; i < int(f.size()); ++i) a[i] = Complex(f[i] >> 15, f[i] & 32767);
    for (int i = 0; i < int(g.size()); ++i) c[i] = Complex(g[i] >> 15, g[i] & 32767);
    dft(a);
    dft(c);
    for (int i = 0; i < n; ++i) b[i] = a[i].conj();
    std::reverse(b.begin() + 1, b.end());
    for (int i = 0; i < n; ++i) d[i] = c[i].conj();
    std::reverse(d.begin() + 1, d.end());
    for (int i = 0; i < n; ++i) {
        Complex aa = (a[i] + b[i]) * Complex(0.5, 0);
        Complex bb = (a[i] - b[i]) * Complex(0, -0.5);
        Complex cc = (c[i] + d[i]) * Complex(0.5, 0);
        Complex dd = (c[i] - d[i]) * Complex(0, -0.5);
        a[i] = aa * cc + Complex(0, 1) * (aa * dd + bb * cc);
        b[i] = bb * dd;
    }
    idft(a);
    idft(b);
    std::vector<int> res(tot);
    for (int i = 0; i < tot; ++i) {
        int t1 = (long long)(a[i].real + 0.5) % p;
        int t2 = (long long)(a[i].imag + 0.5) % p;
        int t3 = (long long)(b[i].real + 0.5) % p;
        res[i] = (((1ll << 30) * t1 + (1ll << 15) * t2 + t3) % p + p) % p;
    }
    res.resize(lim);
    return res;
}

int power(int a, int b, int p) {
    int r = 1;
    while (b) {
        if (b & 1) r = 1ll * r * a % p;
        a = 1ll * a * a % p;
        b >>= 1;
    }
    return r;
}

int find_root(int p) {
    std::vector<int> r;
    int t = p - 1;
    for (int i = 2; i * i <= p - 1; ++i) {
        if (t % i == 0) {
            r.push_back((p - 1) / i);
            while (t % i == 0) t /= i;
        }
    }
    for (int i = 2; i <= p - 1; ++i) {
        bool ok = true;
        for (int j : r) {
            if (power(i, j, p) == 1) {
                ok = false;
                break;
            }
        }
        if (ok) return i;
    }
    return -1;
}

struct Matrix {
    static int N, P;
    int a[3][3];
    Matrix() : a{} {}
    int *operator [](const int k) { return a[k]; }
    const int *operator [](const int k) const { return a[k]; }
    friend Matrix operator *(const Matrix &a, const Matrix &b) {
        Matrix r;
        for (int i = 0; i < N; ++i)
            for (int j = 0; j < N; ++j)
                for (int k = 0; k < N; ++k)
                    r[i][j] = (r[i][j] + 1ll * a[i][k] * b[k][j]) % P;
        return r;
    }
    Matrix operator ^(int y) const {
        Matrix r, x = *this;
        for (int i = 0; i < N; ++i) r[i][i] = 1;
        while (y) {
            if (y & 1) r = r * x;
            x = x * x;
            y >>= 1;
        }
        return r;
    }
};
int Matrix::N, Matrix::P;

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n, k, L, sx, sy, p;
    std::cin >> n >> k >> L >> sx >> sy >> p;
    --sx;
    --sy;
    Matrix::N = n;
    Matrix::P = p;
    Matrix mat;
    for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
            std::cin >> mat[i][j];
    int g = find_root(p);
    std::vector<int> a(k), b(k * 2 - 1);
    int wk = power(g, (p - 1) / k, p);
    int w = 1;
    for (int i = 0; i < k; ++i) {
        Matrix m = mat;
        for (int l = 0; l < n; ++l)
            for (int r = 0; r < n; ++r)
                m[l][r] = (1ll * mat[l][r] * w + int(l == r)) % p;
        a[i] = power(wk, 1ll * i * (i - 1) / 2 % k, p);
        a[i] = 1ll * a[i] * (m ^ L)[sx][sy] % p;
        w = 1ll * w * wk % p;
    }
    for (int i = 0; i < k * 2 - 1; ++i)
        b[i] = power(wk, p - 1 - 1ll * i * (i - 1) / 2 % k, p);
    std::reverse(b.begin(), b.end());
    auto c = mtt(a, b, p, 2 * k - 1);
    std::reverse(c.begin(), c.end());
    int invk = power(k, p - 2, p);
    for (int i = 0; i < k; ++i) {
        c[i] = 1ll * c[i] * power(wk, 1ll * i * (i - 1) / 2 % k, p) % p * invk % p;
        std::cout << c[i] << '\n';
    }
    
    return 0;
}