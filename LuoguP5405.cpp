#include <bits/stdc++.h>

const int P = 998244353;

int plus(const int x, const int y) {
    return (x + y >= P) ? (x + y - P) : (x + y);
}
int times(const int x, const int y) {
    return (long long) x * y % P;
}
int power(int a, int b) {
    int r = 1;
    while (b) {
        if (b & 1) r = times(r, a);
        a = times(a, a);
        b >>= 1;
    }
    return r;
}
int inverse(const int x) {
    return power(x, P - 2);
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    std::vector<std::vector<int>> a(n);
    for (int i = 0; i < n; ++i) {
        int x, y, z;
        std::cin >> x >> y >> z;
        int inv = inverse(x + y + z);
        a[i] = {times(x, inv), times(y, inv), times(z, inv)};
    }
    std::vector<std::vector<int>> e0(n), e1(n);
    for (int i = 0; i < n - 1; ++i) {
        int u, v;
        std::cin >> u >> v;
        --u;
        --v;
        e0[u].push_back(v);
        e1[v].push_back(u);
    }
    std::vector<int> inv(n * 3 + 1);
    inv[0] = 1;
    for (int i = 1; i <= n * 3; ++i) inv[i] = inverse(i);

    std::vector<int> size(n);
    std::vector<std::vector<int>> f(n);
    std::vector<int> t;
    std::function<void(int, int)> dp = [&](int u, int fa) {
        f[u].assign({0, times(1, a[u][0]), times(2, a[u][1]), times(3, a[u][2])});
        size[u] = 1;
        for (int v : e0[u]) {
            if (v != fa) {
                dp(v, u);
                t.assign((size[u] + size[v]) * 3 + 1, 0);
                for (int i = 0; i <= size[u] * 3; ++i)
                    for (int j = 0; j <= size[v] * 3; ++j)
                        t[i + j] = plus(t[i + j], times(f[u][i], f[v][j]));
                std::swap(f[u], t);
                size[u] += size[v];
            }
        }
        for (int v : e1[u]) {
            if (v != fa) {
                dp(v, u);
                t.assign((size[u] + size[v]) * 3 + 1, 0);
                for (int i = 0; i <= size[u] * 3; ++i) {
                    for (int j = 0; j <= size[v] * 3; ++j) {
                        t[i + j] = plus(t[i + j], times(P - 1, times(f[u][i], f[v][j])));
                        t[i] = plus(t[i], times(f[u][i], f[v][j]));
                    }
                }
                std::swap(f[u], t);
                size[u] += size[v];
            }
        }
        for (int i = 0; i <= size[u] * 3; ++i)
            f[u][i] = times(f[u][i], inv[i]);
    };
    dp(0, -1);
    int ans = 0;
    for (int i : f[0]) ans = plus(ans, i);
    std::cout << ans << '\n';
    
    return 0;
}