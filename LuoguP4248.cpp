#include <bits/stdc++.h>

std::vector<int> get_sa(std::string s) {
    s = s + '#';
    int n = s.length();
    std::vector<int> sa(n), rk(n);
    std::iota(sa.begin(), sa.end(), 0);
    std::sort(sa.begin(), sa.end(), [&](int i, int j) { return s[i] < s[j]; });
    rk[sa[0]] = 0;
    for (int i = 1; i < n; ++i)
        rk[sa[i]] = rk[sa[i - 1]] + (s[sa[i]] != s[sa[i - 1]]);

    std::vector<int> tp(n), tax(n);
    int len = 1;
    while (rk[sa[n - 1]] < n - 1) {
        for (int i = 0; i < len; ++i) tp[i] = n - len + i;
        int cnt = len;
        for (int i = 0; i < n; ++i)
            if (sa[i] >= len) tp[cnt++] = sa[i] - len;

        tax.assign(n, 0);
        for (int i = 0; i < n; ++i) ++tax[rk[i]];
        for (int i = 1; i < n; ++i) tax[i] += tax[i - 1];
        for (int i = n - 1; ~i; --i) sa[--tax[rk[tp[i]]]] = tp[i];

        tp = rk;
        rk[sa[0]] = 0;
        for (int i = 1; i < n; ++i) {
            rk[sa[i]] = rk[sa[i - 1]];
            if (tp[sa[i]] != tp[sa[i - 1]] || tp[sa[i] + len] != tp[sa[i - 1] + len])
                ++rk[sa[i]];
        }

        len *= 2;
    }
    sa.erase(std::find(sa.begin(), sa.end(), n - 1));
    
    --n;
    rk.resize(n);
    for (int i = 0; i < n; ++i)
        rk[sa[i]] = i;
    std::vector<int> ht(n);
    for (int i = 0; i < n; ++i)
        rk[sa[i]] = i;
    int k = 0;
    for (int i = 0; i < n; ++i) {
        if (rk[i] == 0) {
            k = 0;
        } else {
            if (k) --k;
            while (s[i + k] == s[sa[rk[i] - 1] + k]) ++k;
            ht[rk[i]] = k;
        }
    }
    return ht;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    std::string s;
    std::cin >> s;
    auto ht = get_sa(s);
    int n = s.length();
    std::vector<int> stk(n + 1), l(n), r(n);
    int top = 0;
    stk[++top] = 0;
    for (int i = 1; i < n; ++i) {
        while (top && ht[stk[top]] > ht[i]) r[stk[top--]] = i;
        l[i] = stk[top];
        stk[++top] = i;
    }
    while (top) r[stk[top--]] = n;
    long long ans = 0;
    for (int i = 1; i < n; ++i) {
        ans += (long long) (r[i] - i) * (i - l[i]) * ht[i];
    }
    ans = (long long) (n - 1) * n * (n + 1) / 2 - 2 * ans;
    std::cout << ans << '\n';
    
    return 0;
}