#include <bits/stdc++.h>

using i64 = long long;

constexpr int P = 1000000007;
constexpr int Inv2 = (P + 1) / 2;
constexpr int Sqrt2 = 59713600;
constexpr int Root1 = (3 + 2 * Sqrt2) % P;
constexpr int Root2 = (3 - 2 * Sqrt2 % P + P) % P;

int power(int a, i64 b) {
    int r = 1;
    while (b) {
        if (b & 1) r = 1ll * r * a % P;
        a = 1ll * a * a % P;
        b >>= 1;
    }
    return r;
}

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int n;
    std::cin >> n;
    int sum1 = 1;
    int sum2 = 1;
    for (int i = 0; i < n; ++i) {
        i64 x;
        std::cin >> x;
        int r1 = power(Root1, x);
        sum1 = (1ll * sum1 * r1 + 1ll * sum1 * power(r1, P - 2)) % P;
        int r2 = power(Root2, x);
        sum2 = (1ll * sum2 * r2 + 1ll * sum2 * power(r2, P - 2)) % P;
    }
    int ans = 1ll * (sum1 + sum2) * Inv2 % P;
    std::cout << ans << '\n';

    return 0;
}