#include <bits/stdc++.h>

bool tax[100];
int sg[21];

int a[21];

int main() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(nullptr);

    int T;
    std::cin >> T;
    while (T--) {
        int n;
        std::cin >> n;
        sg[n - 1] = 0;
        for (int i = n - 2; i >= 0; --i) {
            std::fill(tax, tax + 100, false);
            for (int j = i + 1; j < n; ++j)
                for (int k = j; k < n; ++k)
                    tax[sg[j] ^ sg[k]] = true;
            sg[i] = std::find(tax, tax + 100, false) - tax;
        }

        for (int i = 0; i < n; ++i) std::cin >> a[i];
        int xor_sum = 0;
        for (int i = 0; i < n; ++i)
            if (a[i] % 2)
                xor_sum ^= sg[i];
        if (xor_sum == 0) {
            std::cout << "-1 -1 -1\n0\n";
        } else {
            int cnt = 0;
            int x = -1, y = -1, z = -1;
            for (int i = 0; i < n - 1; ++i) {
                if (a[i]) {
                    for (int j = i + 1; j < n; ++j) {
                        for (int k = j; k < n; ++k) {
                            if ((xor_sum ^ sg[i] ^ sg[j] ^ sg[k]) == 0) {
                                ++cnt;
                                if (cnt == 1) {
                                    x = i;
                                    y = j;
                                    z = k;
                                }
                            }
                        }
                    }
                }
            }
            std::cout << x << ' ' << y << ' ' << z << '\n';
            std::cout << cnt << '\n';
        }
    }

    return 0;
}